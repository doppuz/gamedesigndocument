﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using Mirror;
using UnityEngine.SceneManagement;

/*In questo script definiamo la struttura procedurale della mappa con gli ostacoli e le diverse regioni.*/
//public class Manager : Mirror.NetworkBehaviour {
public class NetworkManagerScript : NetworkBehaviour {

    /*[SerializeField]
    public SyncDictionary data = new SyncDictionary();*/

    //[SerializeField]
    //public readonly SyncDictionaryStringItem Equipment = new SyncDictionaryStringItem();

    public Color gizmoColor = Color.blue;
    public int numberOfObstacle = 130;
    private List<uint> players;
    public List<uint> Players { get => players; set => players = value; }
    public bool IsGameFinished { get => isGameFinished; set => isGameFinished = value; }
    public uint WinningPlayer { get => winningPlayer; set => winningPlayer = value; }

    [SyncVar]
    public GameObject map;

    [SyncVar]
    private bool isGameFinished = false;

    [SyncVar]
    private uint winningPlayer = 0;

    public GameObject obstacle1;
    public GameObject obstacle2;
    public GameObject obstacle3;
    public GameObject waterGO;
    public GameObject climateGO;
    public GameObject robotGO;
    public GameObject terraformerGO;
    public GameObject wellGO;
    public GameObject doggo;
    public GameObject crystalPrefab;
    public GameObject keyPrefab;
    private List<GameObject> obstacles = new List<GameObject>();
    private List<Node> nodesNearWell = new List<Node>();

    /*public GameObject keyPrefab;
    public GameObject crystalPrefab;

    /* obstacleArray e elementsGoarray servono solo come riferimenti al gameobject degli ostacoli e ai prefab delle caselle.*/
    private GameObject[] obstacleArray;
    private GameObject[] elementsGOArray;

    public SA.CardInstance currentCard = null;
    public SA.MyCardsDownAreaLogic areaLogic;

    [SyncVar]
    public uint currentPlayer = 0;

    [SyncVar]
    public int currentIndex = 0;

    [SyncVar]
    public int maxIndex = 0;

    public Graph graph = new Graph();
    private System.Random rnd = new System.Random();
    public bool isMapReady = false;
    public bool firstKeysDrop;
    Node[] playersStartedPosition;

    public override void OnStartServer() {
        base.OnStartServer();
        firstKeysDrop = true;
        Players = new List<uint>();
        obstacleArray = new GameObject[] { obstacle1, obstacle2, obstacle3 };
        elementsGOArray = new GameObject[] { waterGO, terraformerGO, climateGO, robotGO };

        CreateMap();

        placeKeys(3);
        placeCrystals(3);
        firstKeysDrop = false;
    }

    [Command(ignoreAuthority = true)]
    public void CmdAddPlayer(uint id) {
        Players.Add(id);
        currentPlayer = Players[currentIndex];
        maxIndex += 1;
    }

    [Command(ignoreAuthority = true)]
    public void CmdSetClientMap() {
        foreach (GameObject go in obstacles) {
            Vector3 lastPosition = go.transform.position;
            Quaternion lastOrientation = go.transform.rotation;
            string name = go.name;
            Node placeHolder = new Node(-100, -100, go);
            RpcSyncMapCreation(name, lastPosition, lastOrientation, placeHolder, placeHolder.TerrainType);
        }
        foreach (Node node in graph.getNodes()) {
            Vector3 lastPosition = node.Hexagon.transform.position;
            Quaternion lastOrientation = node.Hexagon.transform.rotation;
            string name = node.Hexagon.name;

            RpcSyncMapCreation(name, lastPosition, lastOrientation, node, node.TerrainType);
        }
        RpcCreateGraph();
    }


    [Command(ignoreAuthority = true)]
    public void CmdChangeCurrentPlayer() {
        currentIndex = (currentIndex + 1) % (Players.Count + 1);
        if (currentIndex != Players.Count)
            currentPlayer = Players[currentIndex];
        else
            currentPlayer = 0;
    }

    public override void OnStartClient() {
        base.OnStartClient();
    }

    [Server]
    public void CreateMap() {

        createGraph(true);

        isMapReady = true;

        foreach (Edge e in graph.getConnections(graph.getNode(0, 0)))
            nodesNearWell.Add(e.to);

        changePrefab(wellGO, graph.getNode(0, 0));
        graph.DeleteNode(graph.getNode(0, 0));

        playersStartedPosition = new Node[] { graph.getNode(0, -12), graph.getNode(0, 14), graph.getNode(-11, 0), graph.getNode(12, 0) };

        createObstacle();

        generateMapFaction();
        //faccio partire spawnDog, metto dentro nome game object e dove sarà spawnato
        spawnDoggo();

    }
    [ClientRpc]
    public void RpcSyncMapCreation(string name, Vector3 localPos, Quaternion localRot, Node node, Node.type nodeType) {
        node.Hexagon.name = name;
        node.hexagon.transform.parent = map.transform;
        node.hexagon.transform.localPosition = localPos;
        node.hexagon.transform.localRotation = localRot;
        node.TerrainType = nodeType;
        node.hexagon.tag = "hexagon";
        if (node.x != -100)
            graph.AddNode(node);
    }

    [ClientRpc]
    public void RpcCreateGraph() {
        if (!isMapReady) {
            createGraph(false);
            isMapReady = true;
        }
    }

    [Command(ignoreAuthority = true)]
    public void CmdSetTrap(Node node, bool trapState){
        node.trap = trapState;
        Node n = graph.getNode(node.X, node.Z);
        n.trap = trapState;

        RpcSyncNodeTrap(n, trapState);
    }

    [ClientRpc]
    public void RpcSyncNodeTrap(Node node, bool trapState) {
        
        Node n = graph.getNode(node.X, node.Z);
        n.trap = trapState;
    }

    // Start is called before the first frame update
    void Start() {
        if (isClient && graph.getNodes().Length == 0) {
            CmdSetClientMap();
        }

        obstacleArray = new GameObject[] { obstacle1, obstacle2, obstacle3 };
        elementsGOArray = new GameObject[] { waterGO, terraformerGO, climateGO, robotGO };
    }

    // Update is called once per frame
    void Update() {

    }
    /* Creo il grafo. Ogni casella nera (quella che vedete prima di fare play) hanno come nome la posizione. Quindi prendo quella posizione e creo un nodo alla volta.
     * Poi devo valutare se esistono i miei vicini per collegare un arco. Ci sono due casi perchè in base a che riga si trova la casella i vicini si trovano in posizioni diverse.
     */
    void createGraph(bool server) {
        if (server) {
            foreach (Transform t in map.transform) {
                GameObject tmpGO = t.gameObject;
                String[] tmp = tmpGO.ToString().Split(',');

                graph.AddNode(new Node(int.Parse(tmp[0]), int.Parse(tmp[1].Split(' ')[0]), tmpGO));
            }

        }
        foreach (Node node in graph.getNodes()) {

            if (node.Z % 2 == 0) {

                if (graph.getNode(node.X, node.Z - 1) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X, node.Z - 1)));
                }

                if (graph.getNode(node.X - 1, node.Z - 1) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X - 1, node.Z - 1)));
                }

                if (graph.getNode(node.X - 1, node.Z) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X - 1, node.Z)));
                }

                if (graph.getNode(node.X - 1, node.Z + 1) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X - 1, node.Z + 1)));
                }

                if (graph.getNode(node.X, node.Z + 1) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X, node.Z + 1)));
                }

                if (graph.getNode(node.X + 1, node.Z) != null)
                    graph.AddEdge(new Edge(node, graph.getNode(node.X + 1, node.Z)));

            } else {

                if (graph.getNode(node.X, node.Z - 1) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X, node.Z - 1)));
                }

                if (graph.getNode(node.X + 1, node.Z - 1) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X + 1, node.Z - 1)));
                }

                if (graph.getNode(node.X - 1, node.Z) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X - 1, node.Z)));
                }

                if (graph.getNode(node.X, node.Z + 1) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X, node.Z + 1)));
                }

                if (graph.getNode(node.X + 1, node.Z) != null) {
                    graph.AddEdge(new Edge(node, graph.getNode(node.X + 1, node.Z)));
                }

                if (graph.getNode(node.X + 1, node.Z + 1) != null)
                    graph.AddEdge(new Edge(node, graph.getNode(node.X + 1, node.Z + 1)));

            }
        }
    }
    /*Crea gli ostacoli nella mappa. Genera un numero causale nel grafo. Poi c'è un controllo di un altro metodo che si occupa di verificare che ci siano almeno
     3 caselle libere. Se non ci sono rimette dentro il grafo il nodo e ritenta.*/
    //[Command]
    void createObstacle() {

        List<Node> graphNodeList = graph.getNodesList();
        List<Node> tmpList = graph.getNodesList();

        Node n = graph.getNode(0, 0);
        tmpList.Remove(n);
        foreach (Node node in playersStartedPosition)
            tmpList.Remove(node);

        for (int i = 0; i < numberOfObstacle; i++) {
            int rndNumber = rnd.Next(tmpList.Count);
            Node nodeToRemove = tmpList[rndNumber];

            if (CheckCorrectObstaclePosition(nodeToRemove)) {
                GameObject rock = obstacleArray[rnd.Next(3)];
                changePrefab(rock, nodeToRemove);
                graph.DeleteNode(nodeToRemove);
            } else {
                i -= 1;
            }

            tmpList.Remove(nodeToRemove);
        }
    }

    /*Questo serve per mettere dei vincoli sul dove mettere gli ostacoli.
      In questo caso serve per non occludere il percorso.
      Ogni casella deve sempre avere almeno 3 caselle libere, senò c'è possibilità
      di occlusione.*/
    private bool CheckCorrectObstaclePosition(Node n) {

        foreach (Edge e in graph.getConnections(n)) {
            if (graph.getConnections(e.to).Length <= 3)
                return false;
        }

        return true;
    }

    /* Genera i diversi colori nella mappa sostituendo il gamobject. Non c'è ancora un vero vincolo su come posizionarli, sono messi casualmente per ora.
     */
    void generateMapFaction() {

        int hexagonNumber = (graph.getNodesList().Count) / 4;
        List<Node> tmpList = graph.getNodesList();

        for (int i = 0; i < graph.getNodesList().Count; i++) {
            int rndNumber = rnd.Next(tmpList.Count);
            Node nodeToRemove = tmpList[rndNumber];
            nodeToRemove.TerrainType = (Node.type)((int)i / hexagonNumber);
            changePrefab(elementsGOArray[(int)i / hexagonNumber], nodeToRemove);
            tmpList.Remove(nodeToRemove);
        }
    }

    /*Sostituisce il GO associato al nodo con quello passato come parametro
     */
    private void changePrefab(GameObject go, Node node) {
        Vector3 lastPosition = node.Hexagon.transform.position;
        Quaternion lastOrientation = node.Hexagon.transform.rotation;
        string name = node.Hexagon.name;
        Destroy(node.Hexagon);
        NetworkServer.Destroy(node.Hexagon);
        node.Hexagon = Instantiate(go, lastPosition, lastOrientation);
        node.Hexagon.name = name;
        node.Hexagon.transform.parent = map.transform;
        node.Hexagon.tag = "hexagon";
        NetworkServer.Spawn(node.Hexagon);
        if (go.Equals(wellGO) || obstacleArray.Contains(go))
            obstacles.Add(node.Hexagon);
    }

    public void changePrefabFromCard(Node node, string terrainType){
        GameObject go = null;

        switch(terrainType)
            {
                case "watermutant":
                    go = waterGO;
                break;
                case "climate":
                    go = climateGO;
                break;
                case "terraformer":
                    go = terraformerGO;
                break;
                case "robot":
                    go = robotGO;
                break;
            }
            
        if(go!=null){
            changePrefab(go, node);

        }else{
            return;
        }
    }




    [Command(ignoreAuthority = true)]
    public void CmdPlaceDoggo(GameObject dstr, Node n) {
        graph.getNode(n.X, n.Z).Key = null;
        GameObject.Destroy(dstr);
        NetworkServer.Destroy(dstr);
        spawnDoggo();
    }

    [Command(ignoreAuthority = true)]
    public void CmdPlaceKey(int keysNeeded, GameObject dstr, Node n) {
        graph.getNode(n.X, n.Z).Key = null;
        GameObject.Destroy(dstr);
        NetworkServer.Destroy(dstr);
        placeKeys(keysNeeded);
    }

    [Command(ignoreAuthority = true)]
    public void CmdPlaceCrystal(int crystal, GameObject dstr, Node n) {
        graph.getNode(n.X, n.Z).Crystal = null;
        GameObject.Destroy(dstr);
        NetworkServer.Destroy(dstr);
        placeCrystals(crystal);
    }

    [Server]
    public void placeKeys(int keysNeeded) {

        List<Node> graphNodeList = graph.getNodesList(); //anche io

        List<Node> nodesToRemove = new List<Node>();//io no graph get node

        if (!firstKeysDrop) { //io no compreso l else 
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject go in players) {
                    nodesToRemove.Add(go.GetComponent<PlayerScript>().CurrentNode);
            }
        } else {
            nodesToRemove.Add(graph.getNode(0, -12));
            nodesToRemove.Add(graph.getNode(-11, 0));
            nodesToRemove.Add(graph.getNode(0, 14));
            nodesToRemove.Add(graph.getNode(12, 0));
        }
        for (int i = 0; i < keysNeeded; i++) { 
            int rndNumber = rnd.Next(graphNodeList.Count);
            Node nodeToCheck = graphNodeList[rndNumber];

            if (nodeToCheck.hasKey() == false && nodeToCheck.hasCrystal() == false && !nodesToRemove.Contains(nodeToCheck)) {
                
                nodeToCheck.Key = keyPrefab;
                Vector3 newPosition = nodeToCheck.Hexagon.GetComponent<Renderer>().bounds.center;
                newPosition.y += 0.1f;
                Quaternion orientation = Quaternion.identity;
                nodeToCheck.Key = Instantiate(keyPrefab, newPosition, orientation);
                NetworkServer.Spawn(nodeToCheck.Key);
                //Debug.Log("Key setted in " + nodeToCheck.X + nodeToCheck.Z);

            } else {
                i = i - 1;
            }
        }
    }

    [Server]
    public void placeCrystals(int crystalsNeeded) {

        List<Node> graphNodeList = graph.getNodesList();
        List<Node> tmpList = graph.getNodesList();

        List<Node> nodesToRemove = new List<Node>();

        if (!firstKeysDrop) {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject go in players) {
                nodesToRemove.Add(go.GetComponent<PlayerScript>().CurrentNode);
            }
        } else {
            nodesToRemove.Add(graph.getNode(0, -12));
            nodesToRemove.Add(graph.getNode(-11, 0));
            nodesToRemove.Add(graph.getNode(0, 14));
            nodesToRemove.Add(graph.getNode(12, 0));
        }

        for (int i = 0; i < crystalsNeeded; i++) {
            int rndNumber = rnd.Next(tmpList.Count);
            Node nodeToCheck = tmpList[rndNumber];

            if (nodeToCheck.hasKey() == false && nodeToCheck.hasCrystal() == false && !nodesToRemove.Contains(nodeToCheck)) {
                nodeToCheck.Crystal = crystalPrefab;

                Vector3 newPosition = nodeToCheck.Hexagon.GetComponent<Renderer>().bounds.center;
                Quaternion lastOrientation = Quaternion.identity;
                nodeToCheck.Key = Instantiate(crystalPrefab, newPosition, lastOrientation);
                NetworkServer.Spawn(nodeToCheck.Key);
                //Debug.Log("Crystal setted in " + nodeToCheck.X + nodeToCheck.Z);

            } else {
                i = i - 1;
            }
        }
    }

    [Command(ignoreAuthority = true)]
    public void CmdCheckForWin(Node node) {
        if (nodesNearWell.Contains(node)) {
            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player")) {
                if (player.GetComponent<NetworkIdentity>().netId == currentPlayer) {
                    if (player.GetComponent<Keys>().nKeys == 3) {
                        WinningPlayer = currentPlayer;
                        isGameFinished = true;
                    }
                    break;
                }
            }
        }
    }
   

    void OnDrawGizmos() {
        if (graph != null) {
            Gizmos.color = gizmoColor;
            foreach (Node n in graph.getNodes()) {
                //Debug.Log(graph.getConnections(n).Length);
                foreach (Edge e in graph.getConnections(n)) {
                    Vector3 from = e.from.Hexagon.GetComponent<Renderer>().bounds.center;
                    Vector3 to = e.to.Hexagon.GetComponent<Renderer>().bounds.center;
                    Gizmos.DrawLine(from, to);
                }
            }
        }
    }

    [Server]
    public void spawnDoggo(){

        List<Node> graphNodeList = graph.getNodesList(); 
        List<Node> node = new List<Node>();

        int rndNumber = rnd.Next(graphNodeList.Count);
        Node nodeToCheck = graphNodeList[rndNumber];
        Debug.Log(nodeToCheck.ToString());
        //Debug.Log("il nome del nodo è:  "+ nodeToCheck.TerrainType.ToString());

        if(nodeToCheck.checkNodeType("watermutant") || nodeToCheck.checkNodeType("terraformer") || nodeToCheck.checkNodeType("robot") || nodeToCheck.checkNodeType("climate"))
        {
            if (nodeToCheck.hasKey() == false && nodeToCheck.hasCrystal() == false) 
            {
                Vector3 newPosition = nodeToCheck.Hexagon.GetComponent<Renderer>().bounds.center;
                newPosition.y += 0.1f;
                Quaternion orientation = Quaternion.identity;
                nodeToCheck.Key = Instantiate(doggo, newPosition, orientation);
                NetworkServer.Spawn(nodeToCheck.Key); 
                Debug.Log("SPAWNATO IN " + nodeToCheck.ToString());
            }else{
                int rndNumber1 = rnd.Next(graphNodeList.Count);
                Node nodeToCheck1 = graphNodeList[rndNumber];
                
                if (nodeToCheck.hasKey() == false && nodeToCheck.hasCrystal() == false) 
                {
                    Vector3 newPosition = nodeToCheck.Hexagon.GetComponent<Renderer>().bounds.center;
                    newPosition.y += 0.1f;
                    Quaternion orientation = Quaternion.identity;
                    nodeToCheck.Key = Instantiate(doggo, newPosition, orientation);
                    NetworkServer.Spawn(nodeToCheck.Key); 
                    Debug.Log("SPAWNATO IN " + nodeToCheck.ToString());
                }

                Debug.Log("abbiamo beccato un cristallo o una key");
            }
                
        }else{
            Debug.Log("SPAWNATO DOVE NON DOVREBBE");
        }
    }
}



/*if(nodeToCheck.checkNodeType(t) || nodeToCheck.checkNodeType(w) || nodeToCheck.checkNodeType(c) || nodeToCheck.checkNodeType(r)){
            if (nodeToCheck.hasKey() == false && nodeToCheck.hasCrystal() == false //&& !nodesToRemove.Contains(nodeToCheck)) {

            //unica parte funzionante
            //List<Node> graphNodeList = graph.getNodesList();
            //Node nodeToCheck = graph.getNode(1, 1);
            Vector3 newPosition = nodeToCheck.Hexagon.GetComponent<Renderer>().bounds.center;
            newPosition.y += 0.1f;
            Quaternion orientation = Quaternion.identity;
            nodeToCheck.Key = Instantiate(doggo, newPosition, orientation);
            NetworkServer.Spawn(nodeToCheck.Key); 

            }       
        }*/