﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.IO;
using System.Runtime.Serialization;  
using System.Runtime.Serialization.Formatters.Binary; 

/*Questo script serve solamente per creare la struttura della mappa, come intersezione di un piano e un collider.*/
public class HexagonTileMapGenerator : MonoBehaviour{

    public GameObject HexTilePrefab;
    public int mapWidth;
    public int mapHeight; 
    public float tileXOffset = 1.8f;
    public float tileZOffset = 1.56f;
    public Color gizmoColor = Color.blue;
    public bool serializableGraph = false;

    float mapXMin;
    float mapXMax;
    float mapZMin;
    float mapZMax;
    
    public Graph graph;

    void Start(){        

        mapXMin = -mapWidth / 2;
        mapXMax = mapWidth / 2;
        mapZMin = -mapHeight / 2;
        mapZMax = mapHeight / 2;
        
        graph = new Graph();
        CreateHexTileMap();
    }

    public void CreateHexTileMap(){ 

        for(int x = (int) mapXMin; x <= mapXMax; x++){
            for(int z = (int) mapZMin; z <= mapZMax; z++){
                GameObject tmpGO = Instantiate(HexTilePrefab);
                Vector3 pos;
                
                if(z % 2 == 0){
                    pos = new Vector3(x * tileXOffset,0,z*tileZOffset);
                }else
                    pos = new Vector3(x * tileXOffset + tileXOffset / 2,0,z*tileZOffset);

                tmpGO.transform.position = pos;

                tmpGO.GetComponent<Coordinate>().X = x;
                tmpGO.GetComponent<Coordinate>().Z = z;

                graph.AddNode(new Node(x,z,tmpGO));
                
                tmpGO.transform.position = new Vector3(0,0,0);
                StartCoroutine(SetTileInfo(tmpGO,x,z,pos));    
            }
        }

    }

    IEnumerator SetTileInfo(GameObject go,float x,float z, Vector3 pos){
        yield return new WaitForSeconds(0.0000000001f);
        go.name = x.ToString() + "," + z.ToString();
        go.transform.position = pos;
    }

    void OnTriggerExit(Collider other){
        int x = other.GetComponent<Coordinate>().X;
        int z = other.GetComponent<Coordinate>().Z;
        graph.DeleteNode(new Node(x,z,other.gameObject));
        Destroy(other.gameObject);
    }

    void OnDrawGizmos() {
		if (graph != null) {
			Gizmos.color = gizmoColor;
            foreach(Node n in graph.getNodes()){
                //Debug.Log(graph.getConnections(n).Length);
				foreach (Edge e in graph.getConnections(n)) {
					Vector3 from = e.from.Hexagon.GetComponent<Renderer>().bounds.center;
					Vector3 to = e.to.Hexagon.GetComponent<Renderer>().bounds.center;
					Gizmos.DrawLine (from, to); 
                }
			}
        }
	}

}
