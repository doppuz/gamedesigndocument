﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

/*In questo script definiamo la struttura procedurale della mappa con gli ostacoli e le diverse regioni.*/
public class MapGenerator : MonoBehaviour{

    
    public Color gizmoColor = Color.blue;
    public int numberOfObstacle = 130;
    public GameObject player;

    public GameObject obstacle1;
    public GameObject obstacle2;
    public GameObject obstacle3;
    public GameObject waterGO;
    public GameObject climateGO;
    public GameObject robotGO;
    public GameObject terraformerGO;
    public GameObject wellGO;

    public GameObject keyPrefab;
    public GameObject crystalPrefab;

    public SA.CardInstance currentCardSelected = null;
    public SA.MyCardsDownAreaLogic areaLogic;

    /* obstacleArray e elementsGoarray servono solo come riferimenti al gameobject degli ostacoli e ai prefab delle caselle.*/
    private GameObject[] obstacleArray;
    private GameObject[] elementsGOArray;
    /* playersStartedPosition è un array che memorizza le possibile posizioni in cui posizionare i giocatori. */
    private Node[] playersStartedPosition;

    /*coloredList rappresenta le diverse caselle che sono colorate di rosso quando si preme "s"*/
    private List<Node> coloredList = null;

    /*currentColoredHexagons rappresenta le diverse caselle che sono colorate di nero quando il mouse passa sulle diverse caselle*/
    private List<List<Node>> currentColoredHexagons = null;


    /*lastGO rappresenta l'ultimo GO in cui il mouse è passato. è per non lanciare non ricalcolare il percorso se il mouse è rimasto immobile.*/
    private GameObject lastGO = null;

    /*currentChosenPath serve nella coroutine "WaitForInstruction()". Cliccando il tasto destro possiamo cambiare percorso e al posto di prendere quello
     più breve posso prendere un percorso più lungo (magari per prendere una chiave e un cristallo nello stesso percorso). Il contatore tiene conto di quale
     lista ci stiamo riferendo al momento all'interno di currentColoredHexagons.*/
    private int currentChosenPath = 0;

    /*Serve nell'update per cambiare il colore delle caselle da rosso a bianco. In particolare viene impostata a true quando abbiamo cliccato "s" (e quindi non vogliamo resettare
     * il colore). Viene impostata a false quando finisce il movimento del personaggio.*/
    private bool keyPressed = false;

    private Graph graph;
    private System.Random rnd;
    

    // Start is called before the first frame update
    void Start(){

        //player = PlayerManager.Instance.player;

        rnd = new System.Random();
        graph = new Graph();
        obstacleArray = new GameObject[] { obstacle1, obstacle2, obstacle3 };
        elementsGOArray = new GameObject[] { waterGO, terraformerGO, climateGO, robotGO};

        createGraph();
        playersStartedPosition = new Node[] { graph.getNode(0, -12), graph.getNode(0, 14), graph.getNode(-11, 0), graph.getNode(12, 0) };

        changePrefab(wellGO, graph.getNode(0, 0));
        createObstacle();
        generateMapFaction();

        placeKeys(3);
        placeCrystals(3);

        SetPlayerPosition();

        /*Coroutine (se non sapete cos'è cercate su internet) che permette di di cambiare il path con il tasto destro.*/
        StartCoroutine(WaitForInstruction());
    }

    // Update is called once per frame
    void Update(){
        /*Se il player non si sta muovendo e premo s, calcolo le possibili caselle in cui posso muoversi (per ora è 3). Inoltre resetto i colori delle caselle da un possibile colore
         precedente.*/

        if (!player.GetComponent<PlayerScript>().Move && Input.GetKey("s")) {
            keyPressed = true;
            resetColoredHexagon(coloredList, Color.white);
            coloredList = new List<Node>();
            currentColoredHexagons = null;
            currentChosenPath = 0;
            lastGO = null;
            graph.DepthFirstSearch(player.GetComponent<PlayerScript>().CurrentNode, graph.getNodesList().ToDictionary(x => x, y => false), ref coloredList, 3);
            player.GetComponent<PlayerScript>().CurrentNode.Hexagon.GetComponent<MeshRenderer>().material.color = Color.blue;
        
        } else if (!player.GetComponent<PlayerScript>().Move && Input.GetKey(KeyCode.Escape)) { //se clicco esc resetto il colore a bianco.
            keyPressed = false;
            resetColoredHexagon(coloredList, Color.white);
            coloredList = null;
            currentColoredHexagons = null;
        
        } else if (!player.GetComponent<PlayerScript>().Move && !keyPressed) { //Se il player è fermo e non è stato premuto s, resetto il colore alle caselle.
            resetColoredHexagon(coloredList, Color.white);
            coloredList = null;
            currentColoredHexagons = null;
        }

        /*Se il player non si sta muovendo calcolo il percorso (non importa se non è stato premuto s perchè il raggio funziona solamente
         * se la casella è rossa o nera.)*/
        if(!player.GetComponent<PlayerScript>().Move)
            CheckMouse();

        /*Se premo il tasto sinistro faccio partire il movimento*/
        if (currentColoredHexagons != null && Input.GetMouseButton(0) && !player.GetComponent<PlayerScript>().Move ) {

            player.GetComponent<PlayerScript>().Move = true;
            currentColoredHexagons[currentChosenPath].RemoveAt(0);

            player.GetComponent<PlayerScript>().Path = currentColoredHexagons[currentChosenPath];
            player.GetComponent<PlayerScript>().CurrentNode = currentColoredHexagons[currentChosenPath][0];
           
            keyPressed = false;
        }


        /*Controllo se c'è una carta selezionata quanto ha di range e quanto costa in mana*/
        if(currentCardSelected != null)
        {   
            SA.CardViz v = currentCardSelected.viz;
            if(v.gameObject.activeSelf)
            {
                SA.Card c = v.card;
                string targetNodeType = c.targetNodeType;
                int manaCost = int.Parse(c.properties[2].stringValue);
                int range = int.Parse(c.properties[3].stringValue);
                cardRangeVisualizer(range, targetNodeType); /*coloro di giallo il range in cui può aver effetto la carta*/
            }
        }

        /*Controllo se è stata giocata una carta e se il suo range è > 0 mostro il range di azione*/
        if(areaLogic.playedCard != null && int.Parse(areaLogic.playedCard.viz.card.properties[3].stringValue) > 0)
        {   
            SA.CardViz v = areaLogic.playedCard.viz;
            SA.Card c = v.card;
            string targetNodeType = c.targetNodeType;
            int manaCost = int.Parse(c.properties[2].stringValue);
            int range = int.Parse(c.properties[3].stringValue);
            cardRangeVisualizer(range, targetNodeType); /*coloro solo gli esagoni nel range in cui può aver effetto la carta*/
            
            //se clicco esc resetto il colore a bianco ed esco dalla giocata carte.
            if (Input.GetKey(KeyCode.Escape)) 
            { 
                keyPressed = false;
                resetColoredHexagon(coloredList, Color.white);
                coloredList = null;
                currentColoredHexagons = null;
                areaLogic.playedCard = null;
            }

            Node nodeTarget = CheckMouseForSpecificTile();

            onCardPlayed(c, nodeTarget);
            keyPressed = false;
                resetColoredHexagon(coloredList, Color.white);
                coloredList = null;
                currentColoredHexagons = null;
                areaLogic.playedCard = null;
        }

    }

    /*Resetto il colore delle caselle passate per parametro al colore passato per parametro.*/
    void resetColoredHexagon(List<Node> list, Color color) {
        if(list != null) {
            foreach(Node n in list)
                n.Hexagon.GetComponent<MeshRenderer>().material.color = color;
        }
    }

    /* Creo il grafo. Ogni casella nera (quella che vedete prima di fare play) hanno come nome la posizione. Quindi prendo quella posizione e creo un nodo alla volta.
     * Poi devo valutare se esistono i miei vicini per collegare un arco. Ci sono due casi perchè in base a che riga si trova la casella i vicini si trovano in posizioni diverse.
     */
    void createGraph(){
          
        foreach(Transform t in this.transform){
            GameObject tmpGO = t.gameObject;
            String[] tmp = tmpGO.ToString().Split(',');
            graph.AddNode(new Node(int.Parse(tmp[0]),int.Parse(tmp[1].Split(' ')[0]),tmpGO));
        }

        foreach(Node node in graph.getNodes()){
            
            if(node.Z % 2 == 0){
                
                if(graph.getNode(node.X,node.Z - 1) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X,node.Z - 1)));
                }

                if(graph.getNode(node.X - 1,node.Z - 1) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X - 1,node.Z - 1)));
                }

                if(graph.getNode(node.X - 1,node.Z) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X - 1,node.Z)));
                }

                if(graph.getNode(node.X - 1,node.Z + 1) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X - 1,node.Z + 1)));
                }

                if(graph.getNode(node.X,node.Z + 1) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X,node.Z + 1)));
                }      

                if(graph.getNode(node.X + 1,node.Z) != null)
                    graph.AddEdge(new Edge(node,graph.getNode(node.X + 1,node.Z)));          

            }else{

                if(graph.getNode(node.X,node.Z - 1) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X,node.Z - 1)));
                }

                if(graph.getNode(node.X + 1,node.Z - 1) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X + 1,node.Z - 1)));
                }

                if(graph.getNode(node.X - 1,node.Z) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X - 1,node.Z)));
                }

                if(graph.getNode(node.X,node.Z + 1) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X,node.Z + 1)));
                }

                if(graph.getNode(node.X + 1,node.Z) != null){
                    graph.AddEdge(new Edge(node,graph.getNode(node.X + 1,node.Z)));
                }      

                if(graph.getNode(node.X + 1,node.Z + 1) != null)
                    graph.AddEdge(new Edge(node,graph.getNode(node.X + 1,node.Z + 1))); 

            }
        }
    }

    /*Crea gli ostacoli nella mappa. Genera un numero causale nel grafo. Poi c'è un controllo di un altro metodo che si occupa di verificare che ci siano almeno
     3 caselle libere. Se non ci sono rimette dentro il grafo il nodo e ritenta.*/
    void createObstacle(){

        List<Node> graphNodeList = graph.getNodesList();
        List<Node> tmpList = graph.getNodesList();

        Node n = graph.getNode(0, 0);
        tmpList.Remove(n);
        foreach(Node node in playersStartedPosition)
            tmpList.Remove(node);

        for (int i = 0; i < numberOfObstacle; i++){
            int rndNumber = rnd.Next(tmpList.Count);
            Node nodeToRemove = tmpList[rndNumber];
       
            if (CheckCorrectObstaclePosition(nodeToRemove)){
                GameObject rock = obstacleArray[rnd.Next(3)];
                changePrefab(rock, nodeToRemove);
                graph.DeleteNode(nodeToRemove);
            } else {
                i -= 1;
            }

            tmpList.Remove(nodeToRemove);
        }
    }

    /*Questo serve per mettere dei vincoli sul dove mettere gli ostacoli.
      In questo caso serve per non occludere il percorso.
      Ogni casella deve sempre avere almeno 3 caselle libere, senò c'è possibilità
      di occlusione.*/
    private bool CheckCorrectObstaclePosition(Node n){

        foreach(Edge e in graph.getConnections(n)){
            if (graph.getConnections(e.to).Length <= 3)
                return false;
        }

        return true;
    }

    /* Genera i diversi colori nella mappa sostituendo il gamobject. Non c'è ancora un vero vincolo su come posizionarli, sono messi casualmente per ora.
     */
    void generateMapFaction() {

        int hexagonNumber = (graph.getNodesList().Count - 1) / 4;
        List<Node> tmpList = graph.getNodesList();
        Node n = graph.getNode(0, 0);
        tmpList.Remove(n);

        for (int i = 0; i < graph.getNodesList().Count - 1; i++) {
            int rndNumber = rnd.Next(tmpList.Count);
            Node nodeToRemove = tmpList[rndNumber];
            nodeToRemove.TerrainType = (Node.type)((int)i / hexagonNumber);
            changePrefab(elementsGOArray[(int)i / hexagonNumber], nodeToRemove);
            tmpList.Remove(nodeToRemove);
        }
    }

    /*Sostituisce il GO associato al nodo con quello passato come parametro
     */
    private void changePrefab(GameObject go, Node node) {
        Vector3 lastPosition = node.Hexagon.transform.position;
        Quaternion lastOrientation = node.Hexagon.transform.rotation;
        String name = node.Hexagon.name;
        Destroy(node.Hexagon);
        node.Hexagon = Instantiate(go, lastPosition, lastOrientation);
        node.Hexagon.name = name;
        node.Hexagon.transform.parent = transform;
        node.Hexagon.tag = "hexagon";
    }

    /* Lancio continuamente raggi dall telecamera verso il mouse. Se becco una casella rossa o nera, aggiorno il percorso.*/
    private void CheckMouse() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && (lastGO == null || !lastGO.Equals(hit.collider.gameObject))) {
            lastGO = hit.collider.gameObject;
            if (hit.collider.tag == "hexagon" && (hit.collider.GetComponent<MeshRenderer>().material.color == Color.red ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.black)) {

                List<Node> listWithoutFirst = new List<Node>(coloredList);
                listWithoutFirst.RemoveAt(0);
                resetColoredHexagon(listWithoutFirst, Color.red);

                String[] tmp = hit.collider.gameObject.ToString().Split(',');
                Node startNode = player.GetComponent<PlayerScript>().CurrentNode;
                Node finishNode = graph.getNode(int.Parse(tmp[0]), int.Parse(tmp[1].Split(' ')[0]));
                Dictionary<int, List<Node>> result = new Dictionary<int, List<Node>>();

                graph.searchPath(startNode, finishNode, ref result, 3, new List<Node>() { startNode });

                print(result.Count);

                List<List<Node>> res = new List<List<Node>>(result.Values);
                res = res.OrderBy(x => x.Count).ToList();

                currentColoredHexagons = res;

                for (int i = 1; i < res[0].Count; i++) {
                    res[0][i].Hexagon.gameObject.GetComponent<MeshRenderer>().material.color = Color.black;
                }

                currentChosenPath = 0;

            }
        }
    }

    private void CheckMouseForEnemies() {}

    private Node CheckMouseForSpecificTile() {
        Node fakeNode = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            lastGO = hit.collider.gameObject;
            if (hit.collider.tag == "hexagon" && 
                (hit.collider.GetComponent<MeshRenderer>().material.color == Color.blue ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.yellow ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.green ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.grey)) {

                String[] tmp = hit.collider.gameObject.ToString().Split(',');
                Node finishNode = graph.getNode(int.Parse(tmp[0]), int.Parse(tmp[1].Split(' ')[0]));
                finishNode.Hexagon.gameObject.GetComponent<MeshRenderer>().material.color = Color.black;

                if (Input.GetMouseButton(0)) //quando clicco col mouse ritorno la casella target
                {   
                Debug.Log(finishNode.Hexagon.ToString());
                return finishNode;
                }
            }
        }
        return fakeNode;
    }

    /*Coroutine per cambiare percorso con il tasto destro.*/
    IEnumerator WaitForInstruction() {
        while (true) {
            if (Input.GetMouseButton(1) && !player.GetComponent<PlayerScript>().Move && currentColoredHexagons != null && currentColoredHexagons.Count > 0) {
                List<Node> listWithoutFirst = new List<Node>(coloredList);
                listWithoutFirst.RemoveAt(0);
                resetColoredHexagon(listWithoutFirst, Color.red);
                currentChosenPath = (currentChosenPath + 1) % currentColoredHexagons.Count;
                for (int i = 1; i < currentColoredHexagons[currentChosenPath].Count; i++) {
                    currentColoredHexagons[currentChosenPath][i].Hexagon.gameObject.GetComponent<MeshRenderer>().material.color = Color.black;
                }
            }
            yield return new WaitForSeconds(.15f);
        }
    }

    /* Da provare se avrò tempo.
     * private List<Node> DepthFirstSearchProva(Node start, Dictionary<Node, bool> total, List<Node> visited, int lenght, Material mat) {
        if (lenght == 0) {
            return visited;
        } else {
            total[start] = true;
            visited.Add(start);
            foreach (Edge e in graph.getConnections(start)) {
                if (total[e.to] == false && e.to.Hexagon.GetComponent<MeshRenderer>().material.color == mat.color)
                    DepthFirstSearchProva(e.to, total, visited, lenght - 1, mat);
            }
            return visited;
        }
    }*/

    /*Serve solo per visualizzare il grafo come debug*/
    void OnDrawGizmos() {
        if (graph != null) {
            Gizmos.color = gizmoColor;
            foreach(Node n in graph.getNodes()){
                //Debug.Log(graph.getConnections(n).Length);
                foreach (Edge e in graph.getConnections(n)) {
                    Vector3 from = e.from.Hexagon.GetComponent<Renderer>().bounds.center;
                    Vector3 to = e.to.Hexagon.GetComponent<Renderer>().bounds.center;
                    Gizmos.DrawLine (from, to); 
                }
            }
        }
    }


    public void placeKeys(int keysNeeded){

        List<Node> graphNodeList = graph.getNodesList();

        for (int i = 0; i < keysNeeded; i++){
            int rndNumber = rnd.Next(graphNodeList.Count);    
            Node nodeToCheck = graphNodeList[rndNumber];

            if(nodeToCheck.hasKey() == false && nodeToCheck.hasCrystal() == false){
                nodeToCheck.Key = keyPrefab;

                Vector3 newPosition = nodeToCheck.Hexagon.GetComponent<Renderer>().bounds.center;
                newPosition.y += 0.1f;
                Quaternion orientation = Quaternion.identity;
                nodeToCheck.Key = Instantiate(keyPrefab, newPosition, orientation);
                //Debug.Log("Key setted in " + nodeToCheck.X + nodeToCheck.Z);

            }else{
                i = i-1;
            }
        }
    }

    public void placeCrystals(int crystalsNeeded){

        List<Node> graphNodeList = graph.getNodesList();
        List<Node> tmpList = graph.getNodesList();

        for (int i = 0; i < crystalsNeeded; i++){
            int rndNumber = rnd.Next(tmpList.Count);
            Node nodeToCheck = tmpList[rndNumber];

            if(nodeToCheck.hasKey() == false && nodeToCheck.hasCrystal() == false){
                nodeToCheck.Crystal = crystalPrefab;

                Vector3 newPosition = nodeToCheck.Hexagon.GetComponent<Renderer>().bounds.center;
                Quaternion lastOrientation = Quaternion.identity;
                nodeToCheck.Key = Instantiate(crystalPrefab, newPosition, lastOrientation);
                Debug.Log("Crystal setted in " + nodeToCheck.X + nodeToCheck.Z);

            }else{
                i = i-1;
            }
        }
    }

    public void cardRangeVisualizer(int i, string targetNodeType){
        resetColoredHexagon(coloredList, Color.white);
        coloredList = new List<Node>();
        graph.DepthFirstSearchSpecificTileType(player.GetComponent<PlayerScript>().CurrentNode, graph.getNodesList().ToDictionary(x => x, y => false), ref coloredList, i, targetNodeType);
    }

    private void SetPlayerPosition() {
        int rndNumber = rnd.Next(4);
        Vector3 pos = playersStartedPosition[rndNumber].Hexagon.GetComponent<Renderer>().bounds.center;
        pos.y = 0.25f;
        player.transform.position = pos;
        player.GetComponent<PlayerScript>().CurrentNode = playersStartedPosition[rndNumber];
        Vector3 target = graph.getNode(0f,0f).Hexagon.GetComponent<Renderer>().bounds.center;
        Vector3 destination = (target - player.transform.position).normalized;
        player.transform.rotation = Quaternion.LookRotation(destination);
    }

    public void onCardPlayed(SA.Card c, Node targetNode)
        {   
            switch(c.cardName)
            {
                case "SnailTrail":
                    targetNode.TerrainType = (Node.type)(1);
                    changePrefab(elementsGOArray[1], targetNode);
                    Debug.Log(c.cardName);
                break;

                case "AleaIactaEst": /*muovo di due caselle dove voglio*/

                    keyPressed = true;
                    resetColoredHexagon(coloredList, Color.white);
                    coloredList = new List<Node>();
                    currentColoredHexagons = null;
                    currentChosenPath = 0;
                    lastGO = null;
                    graph.DepthFirstSearch(player.GetComponent<PlayerScript>().CurrentNode, graph.getNodesList().ToDictionary(x => x, y => false), ref coloredList, 2);
                    player.GetComponent<PlayerScript>().CurrentNode.Hexagon.GetComponent<MeshRenderer>().material.color = Color.blue;
        
                    if(!player.GetComponent<PlayerScript>().Move)
                        CheckMouse();

                    if (currentColoredHexagons != null && Input.GetMouseButton(0) && !player.GetComponent<PlayerScript>().Move ) 
                    {
                        player.GetComponent<PlayerScript>().Move = true;
                        currentColoredHexagons[currentChosenPath].RemoveAt(0);

                        player.GetComponent<PlayerScript>().Path = currentColoredHexagons[currentChosenPath];
                        player.GetComponent<PlayerScript>().CurrentNode = currentColoredHexagons[currentChosenPath][0];
           
                        keyPressed = false;
                    }


                    Debug.Log(c.cardName);
                break;

                case "RustyGladius":
                    Debug.Log(c.cardName +""+ targetNode.TerrainType.ToString());
                break;

                case "HumanAvalanche": /*se sono su una casella mountain mi muovo di una*/

                    if(player.GetComponent<PlayerScript>().CurrentNode.checkNodeType("terraformer"))
                    {
                        keyPressed = true;
                        resetColoredHexagon(coloredList, Color.white);
                        coloredList = new List<Node>();
                        currentColoredHexagons = null;
                        currentChosenPath = 0;
                        lastGO = null;
                        graph.DepthFirstSearch(player.GetComponent<PlayerScript>().CurrentNode, graph.getNodesList().ToDictionary(x => x, y => false), ref coloredList, 1);
                        player.GetComponent<PlayerScript>().CurrentNode.Hexagon.GetComponent<MeshRenderer>().material.color = Color.blue;
            
                        if(!player.GetComponent<PlayerScript>().Move)
                            CheckMouse();

                        if (currentColoredHexagons != null && Input.GetMouseButton(0) && !player.GetComponent<PlayerScript>().Move ) 
                        {
                            player.GetComponent<PlayerScript>().Move = true;
                            currentColoredHexagons[currentChosenPath].RemoveAt(0);

                            player.GetComponent<PlayerScript>().Path = currentColoredHexagons[currentChosenPath];
                            player.GetComponent<PlayerScript>().CurrentNode = currentColoredHexagons[currentChosenPath][0];
           
                            keyPressed = false;
                        }
                    }
                break;

                case "PromisedLand":
                    Debug.Log(c.cardName);
                break;

                default:
                    Debug.Log("Che carta è?");
                break;
            }
        }
}
