﻿// vis2k: GUILayout instead of spacey += ...; removed Update hotkeys to avoid
// confusion if someone accidentally presses one.
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class NetworkInterface : MonoBehaviour {

   public NetworkRoomPlayer player;

    public void startHost() {

        NetworkRoomManager manager = NetworkManager.singleton as NetworkRoomManager;
        if (!NetworkClient.isConnected && !NetworkServer.active) 
            manager.StartHost();
    }

    public void startClient() {

        NetworkRoomManager manager = NetworkManager.singleton as NetworkRoomManager;
        if (!NetworkClient.isConnected)
            manager.StartClient();
    }

    public void endGame() {
        NetworkRoomManager manager = NetworkManager.singleton as NetworkRoomManager;
        manager.StopHost();
        Application.Quit();
    }

    /*void StatusLabels() {
        // server / client status message
        if (NetworkServer.active) {
            GUILayout.Label("Server: active. Transport: " + Transport.activeTransport);
        }
        if (NetworkClient.isConnected) {
            GUILayout.Label("Client: address=" + manager.networkAddress);
        }
    }

    void OnGUI() {
        StatusLabels();
    }*/
    
}
