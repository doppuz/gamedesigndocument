﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using UnityEngine.SceneManagement;

public class NetworkManagerRoom : NetworkRoomManager{

    private int index = 0;
    private int i = 0;

    public int Index { get; set; }

    public int I { get; set; }

    public GameObject[] playerPrefabs;

    public bool showStartButton;

    private Color[] colors = { Color.blue, Color.black, Color.green, Color.magenta };

    private Vector3[] startPosition = new Vector3[] {new Vector3(-1.03f,1f, -19.7019f),
                                                     new Vector3(-20.83f,1f, -0.9819069f),
                                                     new Vector3(-1.03f,1f, 20.85809f),
                                                     new Vector3(20.48f,1f, -0.981905f)};

    /* public override void OnRoomServerPlayersReady() {
         // calling the base method calls ServerChangeScene as soon as all players are in Ready state.
         if (isHeadless)
             base.OnRoomServerPlayersReady();
         else
             showStartButton = true;
     }*/

    /*public override void OnGUI() {
        base.OnGUI();

        if (allPlayersReady && showStartButton && GUI.Button(new Rect(150, 300, 120, 20), "START GAME")) {
            // set to false to hide it in the game scene
            showStartButton = false;

            ServerChangeScene(GameplayScene);
        }
    }*/

    public override void OnServerChangeScene(string newSceneName) {
        base.OnServerChangeScene(newSceneName);
        /*if (newSceneName.Contains("NetworkMainScene")) {
            Scene scene = SceneManager.GetSceneByPath(newSceneName);
            print(scene.name);
            print(scene.GetRootGameObjects());
            /*GameObject[] sceneGO = scene.GetRootGameObjects();
            GameObject map = null;
            foreach(GameObject go in sceneGO) {
                if(go.name == "HexagonTileGeneratorNetwork") {
                    map = go;
                    break;
                }
            }
            print(map);
        }*/
    }

    public override GameObject OnRoomServerCreateGamePlayer(NetworkConnection conn, GameObject roomPlayer) {
        GameObject newGO = Instantiate(playerPrefabs[index],startPosition[index],Quaternion.identity);
        newGO.GetComponent<PlayerScript>().name = "Player " + index;
        index += 1;
        return newGO;
    }


}
