﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SpawnerScript : NetworkBehaviour {

    public GameObject manager;

    public override void OnStartServer() {
        base.OnStartServer();
        GameObject managerGO = Instantiate(manager, Vector3.zero, Quaternion.identity);
        NetworkServer.Spawn(managerGO);
    }
}
