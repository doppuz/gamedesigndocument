﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class NetworkRoom : NetworkRoomPlayer {

    public GameObject panel;

    private GameObject prefab;
    private GameObject text;
    private Text t;

    public override void OnClientEnterRoom() {
        base.OnClientEnterRoom();
        prefab = Instantiate(panel, new Vector3(-331 + index * 222, -28, 0), Quaternion.identity) as GameObject;

        prefab.transform.SetParent(GameObject.FindGameObjectWithTag("room").transform, false);
        Button buttonReady = GameObject.Find("ButtonReady").GetComponent<Button>();
        buttonReady.onClick.AddListener(clickReady);
        Button buttonBack = GameObject.Find("ButtonBack").GetComponent<Button>();
        buttonBack.onClick.AddListener(StopConnection);
        NetworkManagerRoom room = NetworkManager.singleton as NetworkManagerRoom;
        prefab.transform.GetChild(0).GetComponent<Text>().text = "Player " + room.I.ToString();
        prefab.name = ""+room.I;
        room.I += 1;
        text = prefab.transform.Find("Ready").gameObject;
    }

    public void clickReady() {
        GameObject text = prefab.transform.Find("Ready").gameObject;
        if (!readyToBegin) {
            CmdChangeReadyState(true);
        } else {
            CmdChangeReadyState(false);
        }
    }

    void StopConnection() {
        NetworkRoomManager room = NetworkManager.singleton as NetworkRoomManager;

        if (NetworkServer.active && NetworkClient.isConnected && isServer) {
            room.StopHost();
        }
            // stop client if client-only
            else if (NetworkClient.isConnected) {
            room.StopClient();
        }
    }

    void Update() {
        NetworkRoomManager room = NetworkManager.singleton as NetworkRoomManager;
        if (NetworkManager.IsSceneActive(room.RoomScene)) {
            if (readyToBegin) {
                text.GetComponent<Text>().text = "ready";
                text.GetComponent<Text>().color = Color.green;
            } else {
                text.GetComponent<Text>().text = "not ready";
                text.GetComponent<Text>().color = Color.red;
            }
        }
    }
    
}