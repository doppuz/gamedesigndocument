﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{	
	[CreateAssetMenu(menuName="Turns/ Control Phase Player")]
	public class ControlPhasePlayer : Phase
	{	

		public GameStates.State playerControlState;

		public override bool IsComplete()
		{	
			if(forceExit)
			{
				forceExit = false;
				return true;
			}

			return false;
		}

		public override void OnEndPhase()
		{	
			if(isInited)
			{	
				Settings.gameManager.SetState(null);
				isInited = false;
			}
		}

		public override void OnStartPhase()
		{
			if(!isInited)
			{	
				Debug.Log(this.name+" starts ");
				Settings.gameManager.SetState(playerControlState);
				Settings.gameManager.onPhaseChanged.Raise();
				isInited = true;
			}
		}
	}
}
