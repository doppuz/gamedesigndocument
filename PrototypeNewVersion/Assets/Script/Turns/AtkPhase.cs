﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{	
	[CreateAssetMenu(menuName="Turns/ Atk Phase Player")]
	public class AtkPhase : Phase
	{
		public override bool IsComplete()
		{	
			if(forceExit)
			{
				forceExit = false;
				return true;
			}

			return false;
		}

		public override void OnEndPhase()
		{	
			if(isInited)
			{	
				Settings.gameManager.SetState(null);
				isInited = false;
			}
		}

		public override void OnStartPhase()
		{
			if(!isInited)
			{	
				Debug.Log(this.name+" starts ");
				Settings.gameManager.SetState(null);
				Settings.gameManager.onPhaseChanged.Raise();
				isInited = true;
			}
		}
	}
}

