﻿
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

[Serializable]
public class Prova {


    public List<List<Edge>> data;

    public Prova() {
        data = new List<List<Edge>>();
        Edge e = new Edge(null, null, 1);
        List<Edge> l = new List<Edge>();
        l.Add(e);
        data.Add(l);
    }

}