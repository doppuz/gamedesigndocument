﻿using System;
using UnityEngine;
using Mirror;

[Serializable]
public class Node{

    [SerializeField]
    public GameObject hexagon;

    [SerializeField]
    public GameObject key;

    [SerializeField]
    public GameObject crystal;

    [SerializeField]
    public int x;

    [SerializeField]
    public int z;

    public enum type { watermutant, terraformer, climate, robot };
    private type terrainType;


    public Node(int x, int z, GameObject o) {
		this.X = x;
		this.Z = z;
		this.hexagon = o;
        terrainType = (Node.type)5;
    }

    public Node() { }

    public int X { get => x; set => x = value; }

    public int Z { get => z; set => z = value; }

    public bool trap = false;

    public GameObject Hexagon{
		get => hexagon;
        set => hexagon = value;
	}

    public type TerrainType { get; set; }
    public GameObject Key { get => key; set => key = value; }
    public GameObject Crystal { get => crystal; set => crystal = value; }

    public override string ToString(){
		return X+" "+Z;
	}

	public override bool Equals(object obj){
		if(obj == null || ! this.GetType().Equals(obj.GetType())){
			return false;
		}else{
			Node n = (Node) obj;
			return this.X == n.X & this.Z == n.Z;
		}
	}

   public override int GetHashCode(){
      return (int)(X * Z) ^ (int)(X*Z);
   }

   public bool hasKey(){
   		if(Key == null){
   			return false;
   		}
   		return true;
   }

    public bool hasCrystal(){
   		if(Crystal == null){
   			return false;
   		}
   		return true;
   }

   public bool checkNodeType(string type){
      if(this.TerrainType.ToString() == type){
        return true;
      }

      return false;
   }

   

   
}