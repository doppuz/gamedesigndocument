﻿
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


[Serializable]
public class SyncListItem : SyncList<Edge> { }

[Serializable]
public class SyncDictionary : SyncDictionary<Node, SyncListItem> { }



[Serializable]
public class Graph{

    
	public Dictionary<Node, List<Edge>> data;

// [SerializeField]
  //  public readonly SyncDictionary data;

    
    public Graph() {
        data = new Dictionary<Node, List<Edge>>();
//        data = dictionary;
	}

	public void AddEdge(Edge e) {
		AddNode (e.from);
		AddNode (e.to);
		if (!data[e.from].Contains(e))
			data [e.from].Add (e);
	}

	// used only by AddEdge 
	public void AddNode(Node n) {
		if (!data.ContainsKey (n))
			data.Add (n, new List<Edge>());
	}


    // returns the list of edged exiting from a node
    public Edge[] getConnections(Node n) {
		if (!data.ContainsKey (n)) return new Edge[0];
		return data [n].ToArray ();
	}

	public Node[] getNodes() {
		return data.Keys.ToArray ();
	}

	public List<Node> getNodesList() {
		return data.Keys.ToList();
	}

	public Node getNode(float x, float z){
		foreach(Node node in getNodes()){
			if(node.X == x & node.Z == z)
				return node;
		}
		return null;
	}

	private void removeEdge(Node n1, Node n2){
		bool find = false;
		int i = 0;
		Edge[] edges = getConnections(n1);

		while(!find && i < edges.Length){
			if(edges[i].to.Equals(n2)){
				data[n1].Remove(edges[i]);
				find = true;
			}
			i++;
		}

	}

	public void DeleteNode(Node n){
		Edge[] edges = getConnections(n);
		for(int i = 0; i < edges.Length; i++){
			removeEdge(edges[i].to,n);
		}
		data.Remove(n);
	}

    public List<Node> DepthFirstSearch(Node start, Dictionary<Node, bool> total, ref List<Node> visited, int depth) {
        if (depth < 0) {
            return visited;
        } else {
            total[start] = true;
            //start.Hexagon.GetComponent<MeshRenderer>().material.color = Color.red;
            if (!visited.Contains(start))
                visited.Add(start);
            foreach (Edge e in getConnections(start)) {
                DepthFirstSearch(e.to, total, ref visited, depth - 1);
            }
            return visited;
        }
    }

    public List<Node> DepthFirstSearchSpecificTileType(Node start, Dictionary<Node, bool> total, ref List<Node> visited, int depth, string targetType) {
        if (depth < 0) {
            return visited;

        } else {         
        		total[start] = true;
        		if(start.checkNodeType(targetType) && targetType == "watermutant")
            		start.Hexagon.GetComponent<MeshRenderer>().material.color = Color.blue;

            	if(start.checkNodeType(targetType) && targetType == "terraformer")
            		start.Hexagon.GetComponent<MeshRenderer>().material.color = Color.yellow;

            	if(start.checkNodeType(targetType) && targetType == "climate")
            		start.Hexagon.GetComponent<MeshRenderer>().material.color = Color.green;

            	if(start.checkNodeType(targetType) && targetType == "robot")
            		start.Hexagon.GetComponent<MeshRenderer>().material.color = Color.grey;

                if(targetType == "enemies"){
                    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                    foreach (GameObject go in players) {
                        if(go.GetComponent<PlayerScript>().CurrentNode.ToString() == start.ToString())
                        {
                            start.Hexagon.GetComponent<MeshRenderer>().material.color = Color.red;
                        }
                    }
                }

                if(targetType == "enemiesMountain"){
                    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                    foreach (GameObject go in players) {
                        if(go.GetComponent<PlayerScript>().CurrentNode.ToString() == start.ToString() && start.checkNodeType("terraformer"))
                        {
                            start.Hexagon.GetComponent<MeshRenderer>().material.color = Color.red;
                        }
                    }
                }

                if(targetType == "anyType")
                    start.Hexagon.GetComponent<MeshRenderer>().material.color = Color.red;               

            	if (!visited.Contains(start))
                	visited.Add(start);
            	foreach (Edge e in getConnections(start)) {
                	DepthFirstSearchSpecificTileType(e.to, total, ref visited, depth - 1, targetType);
            	}

            return visited;
        }
    }

    /*Visita in profondità a partire dal nodo iniziale. Faccio ritornare l'insieme dei nodi totali. La ricerca si spinge fino al massimo della profondità*/
    public List<Node> DepthFirstSearchNew(Node start, Dictionary<Node, bool> total, ref List<Node> visited, int depth, List<Node> nodeToRemove) {
        if (depth < 0) {
            return visited;
        } else {
            if (!nodeToRemove.Contains(start)) {
                total[start] = true;
                if (!visited.Contains(start))
                    visited.Add(start);
                foreach (Edge e in getConnections(start)) {
                    DepthFirstSearchNew(e.to, total, ref visited, depth - 1, nodeToRemove);
                }
            }
        }
        return visited;
    }

    /*Calcola i diversi percorsi dal nodo iniziale a quello finale*/
    public void searchPath(Node start, Node finish, ref Dictionary<int, List<Node>> listNodes, int depth, List<Node> currentList) {
        if (!(depth < 0)) {
            if (start.Equals(finish)) {
                listNodes.Add(listNodes.Count, new List<Node>(currentList));
            } else {
                foreach (Edge e in getConnections(start)) {
                    if (!currentList.Contains(e.to)) {
                        currentList.Add(e.to);
                        searchPath(e.to, finish, ref listNodes, depth - 1, currentList);
                        currentList.Remove(e.to);
                    }
                }
            }
        }
    }

    public void searchPath(Node start, Node finish, ref Dictionary<int, List<Node>> listNodes, int depth, List<Node> currentList, List<Node> nodeToRemove) {
        if (!(depth < 0)) {
            if (start.Equals(finish)) {
                listNodes.Add(listNodes.Count, new List<Node>(currentList));
            } else {
                foreach (Edge e in getConnections(start)) {
                    if (!currentList.Contains(e.to) && !nodeToRemove.Contains(e.to)) {
                        currentList.Add(e.to);
                        searchPath(e.to, finish, ref listNodes, depth - 1, currentList, nodeToRemove);
                        currentList.Remove(e.to);
                    }
                }
            }
        }
    }

}