﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA.GameElements{

	[CreateAssetMenu(menuName="Game Elements/My Card Down")]
	public class MyCardDown : GE_Logic
	{	
		

		public override void OnClick(CardInstance inst)
		{
			Debug.Log("carta giù");
			
		}

		public override void OnHighlight(CardInstance inst)
		{
			Debug.Log("HIGHLIGHT carta giù");
			
		}

	}
}

