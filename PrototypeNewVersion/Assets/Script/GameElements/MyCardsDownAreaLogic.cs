﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{	
	[CreateAssetMenu(menuName="Areas/MyCardsDownWhenHoldingCard")]
	public class MyCardsDownAreaLogic : AreaLogic
	{
		public CardVariable card;
		public string AtkDefCardType;
		public GameObject player;
		public SO.TransformVariable areaGrid;
		public CardInstance playedCard = null;

		public MapGenerator map;

		public string ReturnCardName(string s){

			/*CardInstance inst = card.value;
			if(inst != null){
			Card c = inst.viz.card;
			Debug.Log(c.cardName);
			return c.cardName;
		}
			else{return null;}*/
			return s;
		}
		public override void Execute()
		{
			if(card.value==null){
				ReturnCardName(null);
				return;
			}

			uint currentPlayerId = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().currentPlayer;
			GameObject[] allPlayers = GameObject.FindGameObjectsWithTag("Player");

			foreach(GameObject go in allPlayers){
				Debug.Log(go.GetComponent<PlayerScript>().netId + " " + currentPlayerId);

				if(go.GetComponent<PlayerScript>().netId == currentPlayerId){
					CardInstance inst = card.value;
					playedCard = inst;

					Card c = inst.viz.card;
					int manaCost = int.Parse(c.properties[2].stringValue);
					int manaAvayable = go.GetComponent<Crystals>().mana;

					go.GetComponent<Crystals>().mana = (manaAvayable - manaCost);
					Debug.Log("Place Card Down DEF");
					card.value.transform.SetParent(areaGrid.value.transform);
					Debug.Log(manaAvayable + " - " +manaCost+ " = "+go.GetComponent<Crystals>().mana);
				}
			}

			//onCardPlayed(c);
		}

		/*public void onCardPlayed(Card c)
		{	
			switch(c.cardName)
      		{
          		case "SnailTrail":
              		Debug.Log(c.cardName);
              	break;
          		case "AleaIactaEst":
              		Debug.Log(c.cardName);
              	break;
              	case "RustyGladius":
              		Debug.Log(c.cardName);
              	break;
              	case "HumanAvalanche":
              		Debug.Log(c.cardName);
              	break;
              	case "PromisedLand":
              		Debug.Log(c.cardName);
              	break;
          		default:
              		Debug.Log("Che carta è?");
              		playedCard = null;
		public void onCardPlayed(string cardPlayedName)
		{
			switch(cardPlayedName)
      		{
          		case "SnailTrail":
              		Debug.Log(cardPlayedName);
              		
              	break;
          		case "AleaIactaEst":
              		Debug.Log(cardPlayedName);
              
              	break;
          		default:
              		Debug.Log("Che carta è?");
              		//puntaCarta();
              	break;
      		}
		}*/



		//NEW CODEEEEEEEEEE
		 /*
		public void puntaCarta(){
			Debug.Log("CHIAMATA METODO PUNTACARTA");
			coloredList = new List<Node>();
            currentColoredHexagons = null;
            map.resetColoredHexagon(coloredList, Color.white);
            
            //cerca
            currentChosenPath = 0;
            //lastGO = null;

            //cerca
            //graph.DepthFirstSearch(player.GetComponent<PlayerScript>().CurrentNode, graph.getNodesList().ToDictionary(x => x, y => false), ref coloredList, 3);
            //player.GetComponent<PlayerScript>().CurrentNode.Hexagon.GetComponent<MeshRenderer>().material.color = Color.blue;
        
	       /* if (Input.GetKey(KeyCode.Escape)) { //se clicco esc resetto il colore a bianco.
	            keyPressed = false;
	            resetColoredHexagon(coloredList, Color.white);
	            coloredList = null;
	            currentColoredHexagons = null;
			}
		}
		//NEW CODEEEEEEEEEEEEEEEEEEE 
		}*/

	
	}
}