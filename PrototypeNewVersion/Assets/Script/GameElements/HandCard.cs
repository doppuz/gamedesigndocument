﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA.GameElements
{	
	[CreateAssetMenu(menuName="Game Elements/My Hand Card")]
	public class HandCard : GE_Logic
	{
		public SO.GameEvent onCurrentCardSelected;
		public CardVariable currentCard;
		public SA.GameStates.State holdingCard;
		//public GameObject player;

		Transform transform;

		public override void OnClick(CardInstance inst)
		{	
			uint currentPlayerId = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().currentPlayer;
			GameObject[] allPlayers = GameObject.FindGameObjectsWithTag("Player");

			foreach(GameObject go in allPlayers){

				if(go.GetComponent<PlayerScript>().netId == currentPlayerId){
					Card c = inst.viz.card;
					int manaCost = int.Parse(c.properties[2].stringValue);
					int manaAvayable = go.GetComponent<Crystals>().mana;

					if(manaCost <= manaAvayable){
						currentCard.Set(inst);
						Settings.gameManager.SetState(holdingCard);
						onCurrentCardSelected.Raise();
					}
				}
			}
		}

		public override void OnHighlight(CardInstance inst)
		{								

		}
	}
}
