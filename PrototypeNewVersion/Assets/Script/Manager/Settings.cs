﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SA
{
	public static class Settings
	{
		public static GameManager gameManager;

		private static ResourcesManager _resourcesManagers;

		public static ResourcesManager GetResourcesManager()
		{
			if(_resourcesManagers == null)
			{
				_resourcesManagers = Resources.Load("ResourcesManager") as ResourcesManager;
				_resourcesManagers.Init();

			}
			
		return _resourcesManagers;
		}

		public static List<RaycastResult> GetUIObjs()
		{
			PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
    			position = Input.mousePosition
    		};

    		List<RaycastResult> results = new List<RaycastResult>();
    		EventSystem.current.RaycastAll(pointerData, results);
    		return results;
		}

		public static void SetParentForCard(Transform c, Transform p)
		{
			c.SetParent(p);
			c.localPosition = Vector3.zero;
			c.localEulerAngles = Vector3.zero;
			c.localScale = Vector3.one;
		}
	}
}
