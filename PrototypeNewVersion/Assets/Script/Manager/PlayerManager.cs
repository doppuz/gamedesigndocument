﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
	private static PlayerManager _instance;
	public GameObject player;

	public static PlayerManager Instance
	{	
		private set;
		get;	
	}

	private void awake()
	{
		if(_instance == null)
			_instance = this;
		else
			Destroy(this);
	}

	public void RegisterPlayer(GameObject player){
		this.player = player;
		Debug.Log("PRONTOOOOOOOOOO da PlayerManager");
	}

	public void Unregister(){
		this.player = null;
	}
}
