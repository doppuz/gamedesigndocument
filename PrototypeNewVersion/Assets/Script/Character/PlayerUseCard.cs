﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using Mirror;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*In questo script definiamo la struttura procedurale della mappa con gli ostacoli e le diverse regioni.*/
//public class Manager : Mirror.NetworkBehaviour {
public class PlayerUseCard : NetworkBehaviour {

    //[SerializeField]
    //public readonly SyncDictionaryStringItem Equipment = new SyncDictionaryStringItem();

    /*coloredList rappresenta le diverse caselle che sono colorate di rosso quando si preme "s"*/
    private List<Node> coloredList = null;

    /*currentColoredHexagons rappresenta le diverse caselle che sono colorate di nero quando il mouse passa sulle diverse caselle*/
    private List<List<Node>> currentColoredHexagons = null;


    /*lastGO rappresenta l'ultimo GO in cui il mouse è passato. è per non lanciare non ricalcolare il percorso se il mouse è rimasto immobile.*/
    private GameObject lastGO = null;

    /*currentChosenPath serve nella coroutine "WaitForInstruction()". Cliccando il tasto destro possiamo cambiare percorso e al posto di prendere quello
     più breve posso prendere un percorso più lungo (magari per prendere una chiave e un cristallo nello stesso percorso). Il contatore tiene conto di quale
     lista ci stiamo riferendo al momento all'interno di currentColoredHexagons.*/
    private int currentChosenPath = 0;

    /*Serve nell'update per cambiare il colore delle caselle da rosso a bianco. In particolare viene impostata a true quando abbiamo cliccato "s" (e quindi non vogliamo resettare
     * il colore). Viene impostata a false quando finisce il movimento del personaggio.*/
    private bool keyPressed;

    private bool justSpawned = true;

    private GameObject interfaceCard = null;
    private SA.CardInstance currentCardSelected = null;
    public SA.MyCardsDownAreaLogic areaLogic;

    public Graph graph;
    private Node nodeTarget = null;

    private System.Random rnd = new System.Random();

    //Update is called once per frame
    void Update() {

        if(Input.GetKeyDown("a")){
        	interfaceCard.SetActive(false);
        }

        if(Input.GetKeyDown("z")){
        	interfaceCard.SetActive(true);
        }
        
        if(isLocalPlayer && GameObject.Find("Manager").GetComponent<NetworkManagerScript>().currentPlayer == GetComponent<NetworkIdentity>().netId ){

            graph = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().graph;

            SA.CardViz currentCardViz = currentCardSelected.viz;

            if(currentCardViz != null)
            {   
                if(currentCardViz.gameObject.activeSelf)
                {   
                    SA.Card c = currentCardViz.card;
                    string targetNodeType = c.targetNodeType;
                    int manaCost = int.Parse(c.properties[2].stringValue);
                    int range = int.Parse(c.properties[3].stringValue);
                    cardRangeVisualizer(range, targetNodeType); 
                }
            }

            /*Controllo se è stata giocata una carta e se il suo range è > 0 mostro il range di azione*/
            if(areaLogic.playedCard != null && int.Parse(areaLogic.playedCard.viz.card.properties[3].stringValue) > 0)
            {   
                SA.CardViz v = areaLogic.playedCard.viz;
                SA.Card c = v.card;
                string targetNodeType = c.targetNodeType;
                int manaCost = int.Parse(c.properties[2].stringValue);
                int range = int.Parse(c.properties[3].stringValue);
                cardRangeVisualizer(range, targetNodeType); 

                if(c.cardName!="AleaIactaEst"){
                    nodeTarget = CheckMouseForSpecificTile();
                }else{
                    resetColoredHexagon(coloredList, Color.white);
                    onCardPlayed(c, nodeTarget);
                }
                
                if(nodeTarget!=null){
                    onCardPlayed(c, nodeTarget);
                    keyPressed = false;
                    resetColoredHexagon(coloredList, Color.white);
                    areaLogic.playedCard = null;
                }
            }

            if(areaLogic.playedCard != null && int.Parse(areaLogic.playedCard.viz.card.properties[3].stringValue) == 0){
                SA.CardViz v = areaLogic.playedCard.viz;
                SA.Card c = v.card;
                int manaCost = int.Parse(c.properties[2].stringValue);

                onCardPlayed(c, nodeTarget);
                keyPressed = false;
                areaLogic.playedCard = null;
            }

            //se clicco esc resetto il colore a bianco ed esco dalla giocata carte.
            if(currentCardViz!=null)
                {
                if (areaLogic.playedCard == null && !currentCardViz.gameObject.activeSelf ) //Input.GetKey(KeyCode.Escape)
                {   
                    keyPressed = false;
                    resetColoredHexagon(coloredList, Color.white);
                    coloredList = null;
                    currentColoredHexagons = null;
                    areaLogic.playedCard = null;
                }
            }
        }
    }

    /*Resetto il colore delle caselle passate per parametro al colore passato per parametro.*/
    void resetColoredHexagon(List<Node> list, Color color) {
        if (list != null) {
            foreach (Node n in list)
                CmdChangeColor(n, color);
        }
    }


    /* Lancio continuamente raggi dall telecamera verso il mouse. Se becco una casella rossa o nera, aggiorno il percorso.*/
    private void CheckMouse() {
        Ray ray = gameObject.transform.GetChild(0).gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && (lastGO == null || !lastGO.Equals(hit.collider.gameObject))) {
            lastGO = hit.collider.gameObject;
            if (hit.collider.tag == "hexagon" && (hit.collider.GetComponent<MeshRenderer>().material.color == Color.red ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.black) && coloredList != null) {

                List<Node> listWithoutFirst = new List<Node>(coloredList);
                listWithoutFirst.RemoveAt(0);
                //resetColoredHexagon(listWithoutFirst, Color.red);

                String[] tmp = hit.collider.gameObject.ToString().Split(',');
                Node startNode = GetComponent<PlayerScript>().CurrentNode;
                Node finishNode = graph.getNode(int.Parse(tmp[0]), int.Parse(tmp[1].Split(' ')[0]));
                Dictionary<int, List<Node>> result = new Dictionary<int, List<Node>>();

                List<Node> nodesToRemove = new List<Node>();
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject go in players) {
                    if (go.GetComponent<PlayerScript>().CurrentNode != GetComponent<PlayerScript>().CurrentNode)
                        nodesToRemove.Add(go.GetComponent<PlayerScript>().CurrentNode);
                }

                graph.searchPath(startNode, finishNode, ref result, 3, new List<Node>() { startNode }, nodesToRemove);

                List<List<Node>> res = new List<List<Node>>(result.Values);
                res = res.OrderBy(x => x.Count).ToList();

                currentColoredHexagons = res;

                List<Node> tmpList = new List<Node>(res[0]);
                tmpList.RemoveAt(0);
                resetColoredHexagon(tmpList, Color.black);

                if (Input.GetMouseButton(0)) //quando clicco col mouse ritorno la casella target
                { 
                    nodeTarget = finishNode;
                }

                currentChosenPath = 0;

            }
        }
    }

    private Node CheckMouseForSpecificTile() {
        Node fakeNode = null;
        Ray ray = gameObject.transform.GetChild(0).gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit)) {
            lastGO = hit.collider.gameObject;
            if (hit.collider.tag == "hexagon" && 
                (hit.collider.GetComponent<MeshRenderer>().material.color == Color.blue ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.yellow ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.green ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.grey ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.red)) {

                String[] tmp = hit.collider.gameObject.ToString().Split(',');
                Node finishNode = graph.getNode(int.Parse(tmp[0]), int.Parse(tmp[1].Split(' ')[0]));
                finishNode.Hexagon.gameObject.GetComponent<MeshRenderer>().material.color = Color.black;

                if (Input.GetMouseButton(0)) //quando clicco col mouse ritorno la casella target
                {   
                Debug.Log(finishNode.Hexagon.ToString());
                return finishNode;
                }
            }
        }
        return fakeNode;
    }


    public override void OnStartServer() {
        base.OnStartServer();
    }


    public override void OnStartClient() {
        base.OnStartClient();
    }

    // Start is called before the first frame update
    void Start() {
        currentCardSelected = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().currentCard;
        interfaceCard = GameObject.Find("CardInterface");
        GameObject currentCardObj = GameObject.Find("MouseCardNew");
        if(currentCardObj != null)
            currentCardObj.SetActive(false);
    }

    [Command]
    public void CmdChangeColor(Node node, Color color) {
        resetColoredHexagon2(node, color);
    }

    [ClientRpc]
    void resetColoredHexagon2(Node n, Color color) {
        n.Hexagon.GetComponent<MeshRenderer>().material.color = color;
    }

    public void cardRangeVisualizer(int i, string targetNodeType){
        //resetColoredHexagon(coloredList, Color.white);
        coloredList = new List<Node>();
        graph.DepthFirstSearchSpecificTileType(gameObject.GetComponent<PlayerScript>().CurrentNode, graph.getNodesList().ToDictionary(x => x, y => false), ref coloredList, i, targetNodeType);
    }

    public void onCardPlayed(SA.Card c, Node targetNode)
        {
        switch (c.cardName)
            {
                case "SnailTrail": //trappola su casella acqua
                    targetNode.trap = true; 
                    GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdSetTrap(targetNode, true);
                    Debug.Log(c.cardName);
                    areaLogic.playedCard = null;
                break;

                case "AleaIactaEst": /*muovo nuovamente*/
                    GetComponent<PlayerScript>().moveBonus = true;
                    Debug.Log(GetComponent<PlayerScript>().moveBonus);
                    /*GetComponent<PlayerScript>().CmdsetCurrenstMove(true);
                    GetComponent<PlayerScript>().Move = true;

                    currentColoredHexagons[currentChosenPath].RemoveAt(0);

                    GetComponent<PlayerScript>().Path = currentColoredHexagons[currentChosenPath];

                    GetComponent<PlayerScript>().CurrentNode = currentColoredHexagons[currentChosenPath][0];
                    GetComponent<PlayerScript>().CmdsetCurrentNode(currentColoredHexagons[currentChosenPath][0]);*/

                    Debug.Log(c.cardName);
                    areaLogic.playedCard = null;
                break;

                case "RustyGladius": /*faccio 2 danno ad un player a reange 1*/
                    GameObject[] players1 = GameObject.FindGameObjectsWithTag("Player");
                    GameObject targetPlayer1 = null;

                    foreach (GameObject go in players1) {
                        if(go.GetComponent<PlayerScript>().CurrentNode.ToString() == targetNode.ToString())
                        {
                            targetPlayer1 = go;
                        }
                    }
                    print("AAA " + targetPlayer1);
                    if(targetPlayer1!=null){

                        targetPlayer1.GetComponent<Heart>().CmdChangeHP(-2);
                    }    

                    Debug.Log(c.cardName +""+ targetNode.TerrainType.ToString());
                    areaLogic.playedCard = null;
                break;

                case "HumanAvalanche": /*se sono su una casella mountain mi muovo di una*/

                    if(GetComponent<PlayerScript>().CurrentNode.checkNodeType("terraformer"))
                    {
                        GetComponent<PlayerScript>().moveBonus = true;
                    }

                    Debug.Log(c.cardName);
                break;

                case "PromisedLand": /*piazzo una trappola su una pianura*/
                    targetNode.trap = true; 
                    GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdSetTrap(targetNode, true);
                    Debug.Log(c.cardName);
                    areaLogic.playedCard = null;
                break;

                case "MuskBody": /*recupero 1 cuore se sono su una casella acqua*/
                    if(GetComponent<PlayerScript>().CurrentNode.checkNodeType("watermutant")){
                        //gameObject.GetComponent<Heart>().health = gameObject.GetComponent<Heart>().health + 1;
                        gameObject.GetComponent<Heart>().CmdChangeHP(1);
                    }
                    Debug.Log(c.cardName);
                    areaLogic.playedCard = null;
                break;

                case "StalactiteYouKnowWhere": /*fai 2 danni ad un player che sta su una casella montagna*/
                    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                    GameObject targetPlayer = null;

                    foreach (GameObject go in players) {

                        if(go.GetComponent<PlayerScript>().CurrentNode.ToString() == targetNode.ToString())
                        {
                            targetPlayer = go;
                        }
                    }
                    if(targetPlayer!=null){
                        targetPlayer.GetComponent<Heart>().CmdChangeHP(-2);
                    }
                    areaLogic.playedCard = null;
                break;
                

                default:
                    Debug.Log("Che carta è?");
                break;
            }
        }
}
