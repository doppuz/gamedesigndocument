﻿ using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using Mirror;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class SyncDictionaryStringItem : SyncDictionary<string, string> { }

/*In questo script definiamo la struttura procedurale della mappa con gli ostacoli e le diverse regioni.*/
//public class Manager : Mirror.NetworkBehaviour {
public class PlayerChosenPath : NetworkBehaviour {

    //[SerializeField]
    //public readonly SyncDictionaryStringItem Equipment = new SyncDictionaryStringItem();

    /*coloredList rappresenta le diverse caselle che sono colorate di rosso quando si preme "s"*/
    private List<Node> coloredList = null;

    /*currentColoredHexagons rappresenta le diverse caselle che sono colorate di nero quando il mouse passa sulle diverse caselle*/
    private List<List<Node>> currentColoredHexagons = null;


    /*lastGO rappresenta l'ultimo GO in cui il mouse è passato. è per non lanciare non ricalcolare il percorso se il mouse è rimasto immobile.*/
    private GameObject lastGO = null;

    /*currentChosenPath serve nella coroutine "WaitForInstruction()". Cliccando il tasto destro possiamo cambiare percorso e al posto di prendere quello
     più breve posso prendere un percorso più lungo (magari per prendere una chiave e un cristallo nello stesso percorso). Il contatore tiene conto di quale
     lista ci stiamo riferendo al momento all'interno di currentColoredHexagons.*/
    private int currentChosenPath = 0;

    /*Serve nell'update per cambiare il colore delle caselle da rosso a bianco. In particolare viene impostata a true quando abbiamo cliccato "s" (e quindi non vogliamo resettare
     * il colore). Viene impostata a false quando finisce il movimento del personaggio.*/
    private bool keyPressed;

    private bool justSpawned = true;

    [SyncVar]
    private bool spawnAnimation = false;

    public bool SpawnAnimation { get => spawnAnimation; set => spawnAnimation = value; }

    public Graph graph;

    private GameObject interfaceCard = null;

    [SyncVar]
    public Node spawnNode = null;

    private System.Random rnd = new System.Random();

   
    // Update is called once per frame
    void Update() {
        if (isLocalPlayer && GameObject.Find("Manager").GetComponent<NetworkManagerScript>().currentPlayer == GetComponent<NetworkIdentity>().netId &&
            !GameObject.Find("Manager").GetComponent<NetworkManagerScript>().IsGameFinished) {
            if (!SpawnAnimation) {
                CmdTurnText("Next: "+GetComponent<PlayerScript>().name, "TurnAnimation");
                CmdSpawnAnimation(true);
                GetComponent<PlayerScript>().CmdChangeColor(Color.blue);
                if (GetComponent<CameraManagement>().i == 1)
                    GetComponent<CameraManagement>().LastColor = Color.blue;
            }
            
            /*Se il player non si sta muovendo e premo s, calcolo le possibili caselle in cui posso muoversi (per ora è 3). Inoltre resetto i colori delle caselle da un possibile colore
             precedente.*/
            graph = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().graph;
            if (!GetComponent<PlayerScript>().Move && Input.GetKeyDown("s") && gameObject.transform.GetChild(0).gameObject.activeSelf &&  GetComponent<CameraManagement>().i == 0) {
                interfaceCard.SetActive(false);
                
                keyPressed = true;
                resetColoredHexagon(coloredList, Color.white);
                coloredList = new List<Node>();
                currentColoredHexagons = null;
                currentChosenPath = 0;
                lastGO = null;
                List<Node> nodesToRemove = new List<Node>();
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject go in players) {
                    if (go.GetComponent<PlayerScript>().CurrentNode != GetComponent<PlayerScript>().CurrentNode) 
                        nodesToRemove.Add(go.GetComponent<PlayerScript>().CurrentNode);
                 
                }
                graph.DepthFirstSearchNew(GetComponent<PlayerScript>().CurrentNode, graph.getNodesList().ToDictionary(x => x, y => false), ref coloredList, 3, nodesToRemove);
                resetColoredHexagon(coloredList, Color.red);
                CmdChangeColor(GetComponent<PlayerScript>().CurrentNode,Color.blue);
                //GetComponent<PlayerScript>().CurrentNode.Hexagon.GetComponent<MeshRenderer>().material.color = Color.blue;
            } else if (!GetComponent<PlayerScript>().Move && Input.GetKey(KeyCode.Escape)) { //se clicco esc resetto il colore a bianco.
                keyPressed = false;
                resetColoredHexagon(coloredList, Color.white);
                coloredList = null;
                currentColoredHexagons = null;
            } else if (!GetComponent<PlayerScript>().Move && (!keyPressed || GetComponent<CameraManagement>().i != 0)) { //Se il player è fermo e non è stato premuto s, resetto il colore alle caselle.
                resetColoredHexagon(coloredList, Color.white);
                coloredList = null;
                currentColoredHexagons = null;
            }

            /*Se il player non si sta muovendo calcolo il percorso (non importa se non è stato premuto s perchè il raggio funziona solamente
             * se la casella è rossa o nera.)*/
            if (!GetComponent<PlayerScript>().Move && keyPressed) {
                CheckMouse();
            }

            /*Se premo il tasto sinistro faccio partire il movimento*/
            if (currentColoredHexagons != null && Input.GetMouseButton(0) && !GetComponent<PlayerScript>().Move && !GetComponent<CameraManagement>().MapChange) {

                GetComponent<PlayerScript>().CmdsetCurrentMove(true);
                GetComponent<PlayerScript>().Move = true;

                currentColoredHexagons[currentChosenPath].RemoveAt(0);

                GetComponent<PlayerScript>().Path = currentColoredHexagons[currentChosenPath];

                GetComponent<PlayerScript>().CurrentNode = currentColoredHexagons[currentChosenPath][0];
                GetComponent<PlayerScript>().CmdsetCurrentNode(currentColoredHexagons[currentChosenPath][0]);

                 keyPressed = false;
             }
        } else {
            resetColoredHexagon(coloredList, Color.white);
            coloredList = null;
            currentColoredHexagons = null;
        }
    }

    /*Resetto il colore delle caselle passate per parametro al colore passato per parametro.*/
    public void resetColoredHexagon(List<Node> list, Color color) {
        if (list != null) {
            foreach (Node n in list)
                CmdChangeColor(n, color);
        }
    }


    /* Lancio continuamente raggi dall telecamera verso il mouse. Se becco una casella rossa o nera, aggiorno il percorso.*/
    private void CheckMouse() {
        Ray ray = gameObject.transform.GetChild(0).gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && (lastGO == null || !lastGO.Equals(hit.collider.gameObject))) {
            lastGO = hit.collider.gameObject;
            if (hit.collider.tag == "hexagon" && (hit.collider.GetComponent<MeshRenderer>().material.color == Color.red ||
                hit.collider.GetComponent<MeshRenderer>().material.color == Color.black) && coloredList != null) {

                List<Node> listWithoutFirst = new List<Node>(coloredList);
                listWithoutFirst.RemoveAt(0);
                resetColoredHexagon(listWithoutFirst, Color.red);

                String[] tmp = hit.collider.gameObject.ToString().Split(',');
                Node startNode = GetComponent<PlayerScript>().CurrentNode;
                Node finishNode = graph.getNode(int.Parse(tmp[0]), int.Parse(tmp[1].Split(' ')[0]));
                Dictionary<int, List<Node>> result = new Dictionary<int, List<Node>>();

                List<Node> nodesToRemove = new List<Node>();
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject go in players) {
                    if (go.GetComponent<PlayerScript>().CurrentNode != GetComponent<PlayerScript>().CurrentNode)
                        nodesToRemove.Add(go.GetComponent<PlayerScript>().CurrentNode);
                }

                graph.searchPath(startNode, finishNode, ref result, 3, new List<Node>() { startNode }, nodesToRemove);

                List<List<Node>> res = new List<List<Node>>(result.Values);
                res = res.OrderBy(x => x.Count).ToList();

                currentColoredHexagons = res;

                List<Node> tmpList = new List<Node>(res[0]);
                tmpList.RemoveAt(0);
                resetColoredHexagon(tmpList, Color.black);

                currentChosenPath = 0;

            }
        }
    }

    /*Coroutine per cambiare percorso con il tasto destro.*/
    IEnumerator WaitForInstruction() {
        while (true) {
            if (Input.GetMouseButton(1) && !GetComponent<PlayerScript>().Move && currentColoredHexagons != null && currentColoredHexagons.Count > 0 && isLocalPlayer) {
                List<Node> listWithoutFirst = new List<Node>(coloredList);
                listWithoutFirst.RemoveAt(0);
                resetColoredHexagon(listWithoutFirst, Color.red);
                currentChosenPath = (currentChosenPath + 1) % currentColoredHexagons.Count;
                /*for (int i = 1; i < currentColoredHexagons[currentChosenPath].Count; i++) {
                    currentColoredHexagons[currentChosenPath][i].Hexagon.gameObject.GetComponent<MeshRenderer>().material.color = Color.black;
                }*/
                List<Node> tmpList = new List<Node>(currentColoredHexagons[currentChosenPath]);
                tmpList.RemoveAt(0);
                resetColoredHexagon(tmpList, Color.black);
            }
            yield return new WaitForSeconds(.10f);
        }
    }

    [Command]
    public void CmdSpawnAnimation(bool value) {
        spawnAnimation = value;
    }

    [Command]
    public void CmdTurnText(String text, String component) {
        GameObject.Find("IconInterface").transform.Find(component).GetComponent<Text>().text = text;
        RpcTurnText(text, component);
    }

    [ClientRpc]
    public void RpcTurnText(String text, String component) {
         if (!isLocalPlayer || GameObject.Find("Manager").GetComponent<NetworkManagerScript>().currentIndex == GameObject.Find("Manager").GetComponent<NetworkManagerScript>().maxIndex)
             GameObject.Find("IconInterface").transform.Find(component).GetComponent<Text>().text = text;
         else
             GameObject.Find("IconInterface").transform.Find(component).GetComponent<Text>().text = "Your turn!";
        if (GameObject.Find("IconInterface").GetComponent<Animation>().IsPlaying("FadeIn")) 
            GameObject.Find("IconInterface").GetComponent<Animation>().Stop();
        GameObject.Find("IconInterface").GetComponent<Animation>().Play();
    }



    public override void OnStartServer() {
        base.OnStartServer();
    }


    public override void OnStartClient() {
        base.OnStartClient();
    }

    /*[ClientRpc]
    public void RpcSpawnNode(Node node) {
         spawnNode = node;
    }*/

    [ServerCallback]
    private void OnCollisionEnter(Collision collision) {
        if (justSpawned && collision.gameObject.tag == "hexagon") {
            graph = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().graph;
            String[] tmp = collision.gameObject.ToString().Split(',');
            Node n = graph.getNode(int.Parse(tmp[0]), int.Parse(tmp[1].Split(' ')[0]));
            spawnNode = n;
            gameObject.GetComponent<PlayerScript>().CurrentNode = n;
            justSpawned = false;
        }
    }

    // Start is called before the first frame update
    void Start() {

        /*Coroutine (se non sapete cos'è cercate su internet) che permette di di cambiare il path con il tasto destro.*/
        if (isLocalPlayer) {
            GameObject.Find("IconInterface").transform.Find("TurnText").GetComponent<Text>().text = GetComponent<PlayerScript>().name;
            interfaceCard = GameObject.Find("CardInterface");
            StartCoroutine(WaitForInstruction());
        }

    }

    [Command]
    public void CmdChangeColor(Node node, Color color) {
        resetColoredHexagon2(node, color);
    }

    [ClientRpc]
    void resetColoredHexagon2(Node n, Color color) {
        n.Hexagon.GetComponent<MeshRenderer>().material.color = color;
    }


}
