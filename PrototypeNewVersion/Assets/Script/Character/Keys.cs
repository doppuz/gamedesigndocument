﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using System.Linq;

public class Keys : NetworkBehaviour{

    [SyncVar]
    public int nKeys;

    public GameObject[] keys;
    public Sprite key;

    public MapGenerator map;

    void Start() {
        keys = GameObject.FindGameObjectsWithTag("key");
        List<GameObject> l = new List<GameObject>();
        foreach (GameObject go in keys)
            l.Add(go);
        keys = l.OrderBy(go => go.name).ToArray();
    }

    // Update is called once per frame
    void Update(){
        if (isLocalPlayer) {
            if (nKeys > 3) {
                nKeys = 3;
            }

            for (int i = 0; i < keys.Length; i++) {

                if (i < nKeys) {
                    keys[i].GetComponent<Image>().enabled = true;
                } else {
                    keys[i].GetComponent<Image>().enabled = false;
                }
            }
        }
    }
    
    public void OnCollisionEnter(Collision col){
    	if(col.collider.name == "key_gold(Clone)" && isLocalPlayer){
            GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdPlaceKey(1,col.gameObject,GetComponent<PlayerScript>().CurrentNode);

            if (nKeys <= 3){
                CmdIncreaseKey();
            }
    	}
    }

    [Command]
    public void CmdIncreaseKey() {
        nKeys++;
    }
}

