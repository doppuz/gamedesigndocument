﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class WolfScript :  NetworkBehaviour{

    [SyncVar]
    private bool move = false;

    private List<Node> path = null;
    [SyncVar]
    private Node currentNode = new Node(0,0,null);
    public bool Move { get => move; set => move = value; }
    public List<Node> Path { get => path; set => path = value; }
    public Node CurrentNode { get => currentNode; set => currentNode = value; }
    public float speed;
    public Graph graph;

    // Start is called before the first frame update
    void Start(){
        graph = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().graph;
    }

    [Command(ignoreAuthority = true)]
    public void CmdsetCurrentNode(Node n) {
        currentNode = n;
    }

    [Command(ignoreAuthority = true)]
    public void CmdsetCurrentMove(bool b) {
        move = b;
    }

        // Update is called once per frame
        void Update(){
        if (!GameObject.Find("Manager").GetComponent<NetworkManagerScript>().IsGameFinished) {

            if (move) {
                Vector3 target = currentNode.Hexagon.GetComponent<Renderer>().bounds.center;

                Vector3 destination = (target - transform.position);
                //print("ecco " + destination.magnitude);

                destination.y = 0;
                if (destination.magnitude <= 0.25f) {
                    path.RemoveAt(0);
                    GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdCheckForWin(currentNode);

                    if (path.Count > 0) {
                        CmdsetCurrentNode(path[0]);
                        currentNode = path[0];
                    } else {
                        CmdsetCurrentMove(false);
                        move = false;
                        path = null;
                        GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdChangeCurrentPlayer();
                        GetComponent<WolfChosenPath>().CmdSpawnAnimation(false);
                        GetComponent<WolfChosenPath>().CmdsetPath(false);
                    }
                } else {
                    GetComponent<Rigidbody>().MovePosition(transform.position + destination.normalized * speed * Time.deltaTime);
                }
            }
        }
    }    
}
