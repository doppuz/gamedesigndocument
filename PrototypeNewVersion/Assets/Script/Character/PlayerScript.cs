using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerScript : NetworkBehaviour{

    [SyncVar]
    public string name;

    [SyncVar]
    public Color color = Color.white;

    private Color orange = new Color(0.934f, 0.368f, 0f);

    public override void OnStartServer() {
        base.OnStartServer();
        color = GetComponent<MeshRenderer>().material.color;
    }

    public override void OnStartClient() {
        base.OnStartClient();
        Vector3 lookUpDirection = Vector3.zero - transform.position.normalized;
        transform.rotation = Quaternion.LookRotation(lookUpDirection);
        if (isLocalPlayer) {
            GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdAddPlayer(GetComponent<NetworkIdentity>().netId);
        }
    }

    [SyncVar]
    private bool move = false;

    public bool moveBonus = false;
    public bool snail = false;  

    /*private void OnActiveChange(System.Boolean oldValue, System.Boolean newValue) {
        move = newValue;
        Debug.Log(oldValue+" Active changed on client: " + newValue + " "+GetComponent<NetworkIdentity>().netId);
    }*/

    private List<Node> path = null;

    [SyncVar]
    private Node currentNode = new Node(0,0,null);
    //private Node nodeForRotation = null;

    public bool Move { get => move; set => move = value; }
    public List<Node> Path { get => path; set => path = value; }
    public Node CurrentNode { get => currentNode; set => currentNode = value; }


    public GameObject keyPrefab;
    public GameObject crystalPrefab;
    public float speed;

    public Graph graph;
    private Node spawnNode;

    // Start is called before the first frame update
    void Start(){
        Debug.Log(currentNode);
        graph = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().graph;
    }

    [Command]
    public void CmdChangeColor(Color color) {
        GetComponent<MeshRenderer>().material.color = color;
        RpcChangeColor(color);
    }

    [ClientRpc]
    public void RpcChangeColor(Color color) {
        GetComponent<MeshRenderer>().material.color = color;
    }

    [Command]
    public void CmdName(string name) {
        this.name = name;
    }

    [Command(ignoreAuthority = true)]
    public void CmdsetCurrentNode(Node n) {
        currentNode = n;
    }

    [Command]
    public void CmdsetCurrentMove(bool b) {
        move = b;
    }


    bool pos = false;

    // Update is called once per frame
    void Update(){
        if (isLocalPlayer && !GameObject.Find("Manager").GetComponent<NetworkManagerScript>().IsGameFinished) {
            //GetComponent<MeshRenderer>().material.color = color;
            if (move) {

                Vector3 target = currentNode.Hexagon.GetComponent<Renderer>().bounds.center;
                
                /*if(currentNode.hasKey()){
                    currentNode.key = null;
                    Debug.Log("Chiave trovata");
                }*/

                Vector3 destination = (target - transform.position);
                destination.y = 0;
                if (destination.magnitude <= 0.25f) {
                    path.RemoveAt(0);
                    GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdCheckForWin(currentNode);
                    /*Come potete vedere qui, chiamo sia il metodo del command per move e per currentNode e sia lo cambio manualmente nel client.
                     Questo perchè il command viene eseguito dopo l'update e quindi il nuovo valore per il client non si aggiornerebbe subito ed andrebbe in errore*/
                    if (path.Count > 0) {
                        CmdsetCurrentNode(path[0]);
                        currentNode = path[0];
                    } else {
                        CmdsetCurrentMove(false);
                        move = false;
                        path = null;
                        GetComponent<PlayerChosenPath>().CmdSpawnAnimation(false);
                        GetComponent<PlayerScript>().CmdChangeColor(orange);
                        if (GetComponent<CameraManagement>().i == 1)
                            GetComponent<CameraManagement>().LastColor = orange;

                        if(moveBonus == false){
                            gameObject.GetComponent<Crystals>().mana = gameObject.GetComponent<Crystals>().nCrystals;
                            GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdChangeCurrentPlayer();
                            snail = false;
                        }else{
                            moveBonus = false;
                        }

                    }
                } else {
                    GetComponent<Rigidbody>().MovePosition(transform.position + destination.normalized * speed * Time.deltaTime);
                }
            }
        }

        if(snail){
            if(currentNode.checkNodeType("watermutant") && !currentNode.trap)
                currentNode.trap = true;
            //GameObject.Find("Manager").GetComponent<NetworkManagerScript>().changePrefabFromCard(currentNode, "watermutant");
        }

        graph = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().graph;

        if(graph.getNode(currentNode.X, currentNode.Z) != null && graph.getNode(currentNode.X, currentNode.Z).trap){
            
            currentNode.trap = false;
            graph.getNode(currentNode.X, currentNode.Z).trap = false;
            GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdSetTrap(currentNode, false);

            Debug.Log(currentNode.ToString() + " " + currentNode.trap);
            gameObject.GetComponent<Heart>().health = gameObject.GetComponent<Heart>().health - 1;
            gameObject.GetComponent<Heart>().CmdChangeHP(-1);
        }
        if (gameObject.GetComponent<Heart>().health <= 0) {
                dead();
        }
    }


    public void dead(){
        //spawna n chiavi pari al n di chiavi del player morto
        //metti il player nella sua posizione di partenza 
        //resetta il mana e il n di cristalli

        gameObject.GetComponent<Heart>().health = 5;
        gameObject.GetComponent<Crystals>().mana = 3;
        gameObject.GetComponent<Crystals>().nCrystals = 3;
        Debug.Log("sei morto");

        GameObject.Find("Manager").GetComponent<NetworkManagerScript>().placeKeys(gameObject.GetComponent<Keys>().nKeys);
        gameObject.GetComponent<Keys>().nKeys = 0;    

        Node n = gameObject.GetComponent<PlayerChosenPath>().spawnNode;

        //gameObject.transform.position = new Vector3(n.Hexagon.transform.position.x, gameObject.transform.position.y, n.Hexagon.transform.position.z);

        //currentNode = n;
        //CmdsetCurrentNode(n);

        gameObject.GetComponent<PlayerChosenPath>().resetColoredHexagon(graph.getNodesList(), Color.white);
        CmdsetCurrentMove(false);
        GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdChangeCurrentPlayer();
        //se questo è il current devo far passare il turno
    }

    public void OnCollisionEnter(Collision col){

        if(col.collider.name == "wolfPrefab(Clone)" && isLocalPlayer) {

            Node n = gameObject.GetComponent<PlayerChosenPath>().spawnNode;
            gameObject.transform.position = new Vector3(n.Hexagon.transform.position.x, gameObject.transform.position.y, n.Hexagon.transform.position.z);

            currentNode = n;
            CmdsetCurrentNode(n);
            dead();
        }
    }
}
