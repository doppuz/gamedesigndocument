﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class PlayerStatsInterface : NetworkBehaviour{

    public GameObject label;

    private GameObject canvas;
    private GameObject endGameScreen;
    private bool active = false;
    private int numberPlayers;

    private void Start() {
        canvas = GameObject.Find("IconInterface").transform.GetChild(18).gameObject;
        endGameScreen = GameObject.Find("IconInterface").transform.GetChild(19).gameObject;
    }

    // Update is called once per frame
    void Update(){
        if (isLocalPlayer && GameObject.Find("Manager").GetComponent<NetworkManagerScript>().IsGameFinished) {
            if (active)
                canvas.SetActive(false);
            if (GameObject.Find("Manager").GetComponent<NetworkManagerScript>().WinningPlayer == GetComponent<NetworkIdentity>().netId)
                endGameScreen.transform.GetChild(0).GetComponent<Text>().text = "You win!";
            endGameScreen.SetActive(true);
        } else {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            numberPlayers = players.Length;
            if (isLocalPlayer & Input.GetKeyDown(KeyCode.Tab)) {
                canvas.SetActive(!active);
                for (int i = 0; i < numberPlayers; i++)
                    canvas.transform.GetChild(i).gameObject.SetActive(!active);
                active = !active;
            }

            if (isLocalPlayer && active) {
                for (int i = 0; i < numberPlayers; i++) {
                    Transform tmpLabel = canvas.transform.Find("" + i);
                    tmpLabel.Find("PlayerName").GetComponent<Text>().text = players[i].GetComponent<PlayerScript>().name;
                    tmpLabel.Find("CrystalNumber").GetComponent<Text>().text = players[i].GetComponent<Crystals>().nCrystals.ToString();
                    tmpLabel.Find("KeyNumber").GetComponent<Text>().text = players[i].GetComponent<Keys>().nKeys.ToString();
                    tmpLabel.Find("HeartNumber").GetComponent<Text>().text = players[i].GetComponent<Heart>().health.ToString();
                }
            }
        }
    }
}
