 using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using System.Linq;

public class Heart : NetworkBehaviour{

    [SyncVar]
    public int health;

    public int nHearts;

    public GameObject[] hearts;

    public Sprite fullHeart;
    public Sprite emptyHeart;

    void Start() {
        hearts = GameObject.FindGameObjectsWithTag("heart");
        List<GameObject> l = new List<GameObject>();
        foreach (GameObject go in hearts)
            l.Add(go);
        hearts = l.OrderBy(go => go.name).ToArray();
    }

    // Update is called once per frame
    void Update(){
        if (isLocalPlayer) {
            if (health > nHearts) {
                health = nHearts;
            }

            for (int i = 0; i < hearts.Length; i++) {

                if (i < health) {
                    hearts[i].GetComponent<Image>().sprite = fullHeart;
                } else {
                    hearts[i].GetComponent<Image>().sprite = emptyHeart;
                }


                if (i < nHearts) {
                    hearts[i].GetComponent<Image>().enabled = true;

                } else {
                    hearts[i].GetComponent<Image>().enabled = false;
                }
            }
        }
    }

    [Command(ignoreAuthority = true)]
    public void CmdChangeHP(int i) {
        health = health + i;
    }
}


