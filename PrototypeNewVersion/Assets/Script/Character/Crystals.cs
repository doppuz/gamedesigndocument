using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using System;
using System.Linq;


public class Crystals : NetworkBehaviour{

    [SyncVar]
    public int mana = 3;

    [SyncVar]
    public int nCrystals = 3;
    public GameObject[] crystals;

    public Sprite fullCrystal;
    public Sprite emptyCrystal;

    void Start() {
        crystals = GameObject.FindGameObjectsWithTag("crystal");
        //non avevo voglia di perdere tempo ad ordinare un array, allora ho ordinato una lista che è più semplice. (Fatto questo perchè apparivano sparse nel gioco buildato).
        List<GameObject> l = new List<GameObject>();
        foreach(GameObject go in crystals)
            l.Add(go);
        crystals = l.OrderBy(go => go.name).ToArray();
    }

    // Update is called once per frame
    void Update(){

        if (isLocalPlayer) {
            if (mana > nCrystals) {
                mana = nCrystals;
            }

            for (int i = 0; i < crystals.Length; i++) {

                if (i < mana) {
                    crystals[i].GetComponent<Image>().sprite = fullCrystal;
                } else {
                    crystals[i].GetComponent<Image>().sprite = emptyCrystal;
                }


                if (i < nCrystals) {
                    crystals[i].GetComponent<Image>().enabled = true;

                } else {
                    crystals[i].GetComponent<Image>().enabled = false;
                }
            }
        }
    }


    public void OnCollisionEnter(Collision col){

    	if(col.collider.name == "crystal_17_2(Clone)" && isLocalPlayer) {
            GameObject.Find("Manager").GetComponent<NetworkManagerScript>().CmdPlaceCrystal(1, col.gameObject, GetComponent<PlayerScript>().CurrentNode);

            if(nCrystals <= 7){
                CmdIncreaseCrystal();
            }
    	}
    }

    [Command]
    public void CmdIncreaseCrystal() {
        nCrystals++;
        mana++;
    }
}

