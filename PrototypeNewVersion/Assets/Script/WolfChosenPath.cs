﻿ using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using Mirror;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]

public class WolfChosenPath : NetworkBehaviour {

    private bool justSpawned = true;

    [SyncVar]
    private bool spawnAnimation = false;

    [SyncVar]
    private bool setPath = false;

    public bool SpawnAnimation { get => spawnAnimation; set => spawnAnimation = value; }
    public bool SetPath { get => setPath; set => setPath = value; }

    public Graph graph;

    public List<Node> playersNodes;


    // Update is called once per frame
    void Update() {
        graph = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().graph;
        
        if (GameObject.Find("Manager").GetComponent<NetworkManagerScript>().currentIndex == GameObject.Find("Manager").GetComponent<NetworkManagerScript>().maxIndex &&
        GameObject.Find("Manager").GetComponent<NetworkManagerScript>().currentIndex != 0 && !GameObject.Find("Manager").GetComponent<NetworkManagerScript>().IsGameFinished &&
        !SetPath) {

            if (!SpawnAnimation) {
                PlayerChosenPath p = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerChosenPath>();
                p.CmdTurnText("Wolf Turn", "TurnAnimation");
                CmdSpawnAnimation(true);
            }
            List<Node> list = new List<Node>();
            List<Node> nodesToRemove = new List<Node>();
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject go in players) {
                if (go.GetComponent<PlayerScript>().CurrentNode != GetComponent<WolfScript>().CurrentNode)
                    nodesToRemove.Add(go.GetComponent<PlayerScript>().CurrentNode);

            }
            List<Node> exploredNodes = graph.DepthFirstSearchNew(GetComponent<WolfScript>().CurrentNode, graph.getNodesList().ToDictionary(x => x, y => false), ref list, 3, nodesToRemove);

            Node startNode = GetComponent<WolfScript>().CurrentNode;
            Node finishNode = list[UnityEngine.Random.Range(0, list.Count - 1)];
            //Debug.Log("primo finish" + finishNode.ToString());
            Dictionary<int, List<Node>> result = new Dictionary<int, List<Node>>();

            //graph.searchPath(startNode, finishNode, ref result, 3, new List<Node>() { startNode }, nodesToRemove);

            //
            /*
            foreach(GameObject go in players){
                Debug.Log("Quando esegue questo?");
                playersNodes.Add(go.GetComponent<PlayerScript>().CurrentNode);
            } 

            foreach(Node n in playersNodes){

                if(exploredNodes.Contains(n)){
                    finishNode = n;
                    Debug.Log("secondo finish" + finishNode.ToString());
                }
            }*/
            //

            graph.searchPath(startNode, finishNode, ref result, 3, new List<Node>() { startNode }, nodesToRemove);
            //playersNodes = null;

            List<Node> path = result[0];

            path.RemoveAt(0);

            GetComponent<WolfScript>().Path = path;

            GetComponent<WolfScript>().CurrentNode = path[0];
            GetComponent<WolfScript>().CmdsetCurrentNode(path[0]);

            GetComponent<WolfScript>().CmdsetCurrentMove(true);
            GetComponent<WolfScript>().Move = true;

            setPath = true;
            CmdsetPath(true);
        }
    }

    [Command(ignoreAuthority = true)]
    public void CmdSpawnAnimation(bool value) {
        spawnAnimation = value;
    }

    [Command(ignoreAuthority = true)]
    public void CmdsetPath(bool value) {
        setPath = value;
    }

    [ServerCallback]
    private void OnCollisionEnter(Collision collision) {
        if (justSpawned && collision.gameObject.tag == "hexagon") {
            graph = GameObject.Find("Manager").GetComponent<NetworkManagerScript>().graph;
            String[] tmp = collision.gameObject.ToString().Split(',');
            Node n = graph.getNode(int.Parse(tmp[0]), int.Parse(tmp[1].Split(' ')[0]));
            gameObject.GetComponent<WolfScript>().CurrentNode = n;
            justSpawned = false;
        } else if (collision.gameObject.tag != "hexagon") {
            Physics.IgnoreCollision(collision.collider, GetComponent<BoxCollider>(), true);
        }
    }

}
