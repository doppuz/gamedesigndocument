﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SA;
using SO.UI;

namespace SA
{
	public class UpdateTextFromPhase : UIPropertyUpdater
    {
        public PhaseVariable currentPhase;
        public Text targetText;
        
        public override void Raise()
        {
            targetText.text = currentPhase.value.phaseName;
        }
    }
}

