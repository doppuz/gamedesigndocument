﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


namespace SA.GameStates
{	
	[CreateAssetMenu(menuName = "Actions/MouseHoldWithCard")]
	public class MouseHoldWithCard : Action
	{	
		public CardVariable currentCard;
		public State playerControlState;
		public SO.GameEvent onPlayerControlState;

		public override void Execute(float d)
		{
			bool mouseIsDown = Input.GetMouseButton(0);

			if(!mouseIsDown)
			{	
				List<RaycastResult> results = Settings.GetUIObjs();

				bool isDroppedOnArea = false;

				foreach(RaycastResult r in results)
				{
					GameElements.Area a = r.gameObject.GetComponentInParent<GameElements.Area>();
					
					if(a != null)
					{	
						isDroppedOnArea = true;
						a.OnDrop();
						break;
					}
				}


				if(!isDroppedOnArea)
				{
					currentCard.value.gameObject.SetActive(true);
				}
				else
				{
					currentCard.value = null;
				}

				
				

				Settings.gameManager.SetState(playerControlState);
				onPlayerControlState.Raise();
				return;
			}
		}
	}	
}