﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coordinate : MonoBehaviour{

    private int x = 0;
    private int z = 0;

    public int X{
		get => x; 
        set => x = value;
	}

	public int Z{
		get => z; 
        set => z = value;
	}
   
}
