﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{	
	[CreateAssetMenu(menuName="Holders/Player Holder")]
	public class PlayerHolder : ScriptableObject
	{	

		//parte modificata, decommentare in caso di errore 
		public string[] startingCards;
		//parte nuova
		/*
		public List<string> stcd ..artingDeck = new List<string>();
		[System.NonSerialized]
		public List<string> all_cards = new List<string>();

		public void Init(){

			//health = 20;
			//Add Range?!
			all_cards.AddRange(startingDeck);
		}
		*/



		public SO.TransformVariable handGrid;
		public SO.TransformVariable downGrid;

		public SA.GameElements.GE_Logic handLogic;
		public SA.GameElements.GE_Logic downLogic;

		[System.NonSerialized]
		public List<CardInstance> handCards = new List<CardInstance>();

	}
}
