﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class CameraManagement : NetworkBehaviour{

    public float transitionSpeed;

    private GameObject fullmap;
    private GameObject playerCamera;
    private Vector3 currentDestination;
    private Vector3 currentOrientation;
    private Vector3 lastDestination;
    private Vector3 lastOrientation;

    private Vector3[] destination;
    private bool mapChange = false;
    private bool changeDirection = false;
    private GameObject[] fixedCams;
    private int index;

    private Color lastColor;


    public int i = 0;

    public Color LastColor { set => lastColor = value; }
    public bool MapChange { get => mapChange; set => mapChange = value; }

    public override void OnStartClient() {
        fullmap = GameObject.Find("MainCamera");
    }

    public override void OnStartServer() {
        fullmap = GameObject.Find("MainCamera");
    }

    public void Start() {
        //fullmap = GameObject.Find("MainCamera");
        //fullmap.SetActive(false);
        fixedCams = GameObject.FindGameObjectsWithTag("FixedCamera");
        playerCamera = gameObject.transform.GetChild(0).gameObject;
        destination = new Vector3[] { gameObject.transform.GetChild(0).position, new Vector3(playerCamera.transform.position.x,
            playerCamera.transform.position.y + 22.3f, playerCamera.transform.position.z - 10) };
        currentDestination = destination[0];
        //currentOrientation = desti
        if (isLocalPlayer) {
            playerCamera.SetActive(true);
            fullmap.SetActive(false);
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            for(int i = 0; i < players.Length; i++) {
                if (players[i].GetComponent<NetworkIdentity>().netId == GetComponent<NetworkIdentity>().netId) {
                    index = i;
                    break;
                }
            }
        }
    }

    // Update is called once per frame
    void Update(){
        if (isLocalPlayer && GetComponent<Rigidbody>().velocity.magnitude == 0) {
           if (Input.GetKeyUp("c") && !MapChange && !changeDirection) {
                if (i == 0) {
                    lastColor = GetComponent<MeshRenderer>().material.color;
                    GetComponent<MeshRenderer>().material.color = Color.yellow;
                    currentDestination = fixedCams[4].transform.position;
                    currentOrientation = new Vector3(fixedCams[4].transform.eulerAngles.x, fixedCams[4].transform.eulerAngles.y, fixedCams[4].transform.eulerAngles.z);
                    lastDestination = gameObject.transform.GetChild(0).position;
                    lastOrientation = new Vector3(gameObject.transform.GetChild(0).eulerAngles.x, gameObject.transform.GetChild(0).eulerAngles.y, gameObject.transform.GetChild(0).eulerAngles.z);
                } else {
                    GetComponent<MeshRenderer>().material.color = lastColor;
                    currentDestination = lastDestination;
                    currentOrientation = lastOrientation;
                }
                i = (i + 1) % destination.Length;
                MapChange = true;
            }else if (Input.GetKeyUp(KeyCode.RightArrow) && !MapChange && !changeDirection && i == 0) {
                Transform camLoc = gameObject.transform.GetChild(0);
                currentDestination = new Vector3(-camLoc.localPosition.z, camLoc.localPosition.y, camLoc.localPosition.x);
                currentOrientation = new Vector3(camLoc.localEulerAngles.x, camLoc.localEulerAngles.y - 90, camLoc.localEulerAngles.z);
                changeDirection = true;
            } else if (Input.GetKeyUp(KeyCode.LeftArrow) && !MapChange && !changeDirection && i == 0) {
                Transform camLoc = gameObject.transform.GetChild(0);
                currentDestination = new Vector3(camLoc.localPosition.z, camLoc.localPosition.y, -camLoc.localPosition.x);
                currentOrientation = new Vector3(camLoc.localEulerAngles.x, camLoc.localEulerAngles.y + 90, camLoc.localEulerAngles.z);
                changeDirection = true;
            }
        }
    }

    void LateUpdate() {

        if (isLocalPlayer) {

            if (MapChange) {
                playerCamera.transform.position = Vector3.Lerp(playerCamera.transform.position, currentDestination, Time.deltaTime * transitionSpeed);
                playerCamera.transform.eulerAngles = new Vector3(Mathf.LerpAngle(playerCamera.transform.eulerAngles.x, currentOrientation.x, Time.deltaTime * transitionSpeed),
                        Mathf.LerpAngle(playerCamera.transform.eulerAngles.y, currentOrientation.y, Time.deltaTime * transitionSpeed),
                        Mathf.LerpAngle(playerCamera.transform.eulerAngles.z, currentOrientation.z, Time.deltaTime * transitionSpeed));
                if (Vector3.SqrMagnitude(playerCamera.transform.position - currentDestination) < 0.00000001f) {
                    MapChange = false;
                }
            }

            if (changeDirection) {
                playerCamera.transform.localPosition = Vector3.Lerp(playerCamera.transform.localPosition, currentDestination, Time.deltaTime * transitionSpeed);
                playerCamera.transform.localEulerAngles = new Vector3(Mathf.LerpAngle(playerCamera.transform.localEulerAngles.x, currentOrientation.x, Time.deltaTime * transitionSpeed),
                        Mathf.LerpAngle(playerCamera.transform.localEulerAngles.y, currentOrientation.y, Time.deltaTime * transitionSpeed),
                        Mathf.LerpAngle(playerCamera.transform.localEulerAngles.z, currentOrientation.z, Time.deltaTime * transitionSpeed));
                if (Vector3.SqrMagnitude(playerCamera.transform.localPosition - currentDestination) < 0.00000001f) {
                    changeDirection = false;
                }
            }
        }
        
    }

}
