﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace SA{
	public class CardViz : MonoBehaviour
	{
	    /*public Text title;
	    public Text detail;
	    public Text manaCost;
	    public Text range;
	    public Image art;
	    public Image frame;
	    public Image sound;
	    public Image atkdef;*/

	    public Card card;
	    public CardVizProperties[] properties;


	    public void LoadCard(Card c){

	    	if(c == null){
	    		return;
	    	}

	    	card = c;

	    	c.cardType.OnSetType(this);

	    	for( int i =0; i<c.properties.Length; i++)
	    	{
	    		CardProperties cp = c.properties[i];
	    		CardVizProperties p = GetProperty(cp.element);

	    		if(p == null)
	    			continue;
	    		
	    		if(cp.element is ElementInt)
	    		{
	    			p.text.text = cp.intValue.ToString();
	    		}
	    		else
	    		if(cp.element is ElementText)
	    		{
	    			p.text.text = cp.stringValue;
	    		}
	    		else
	    		if(cp.element is ElementImage)
	    		{
	    			p.img.sprite = cp.sprite;
	    		}
	    	}

	    	/*
	    	title.text = c.cardName;
	    	detail.text = c.cardDetail;
	    	art.sprite = c.art;
	    	manaCost.text = c.manaCost.ToString();
	    	range.text = c.range.ToString();
	    	frame.sprite = c.cardType;
	    	sound.sprite = c.soundLocation;
	    	atkdef.sprite = c.AtkDef;*/
	    }

	    public CardVizProperties GetProperty(Element e)
	    {
	    	CardVizProperties result = null;

	    	for(int i=0; i<properties.Length; i++)
	    	{
	    		if(properties[i].element == e)
	    		{
	    			result = properties[i];
	    			break;
	    		}
	    	}

	    	return result;
	    }
	}
}