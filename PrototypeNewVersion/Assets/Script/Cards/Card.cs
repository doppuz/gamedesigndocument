﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA{

	[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
	public class Card : ScriptableObject
	{
		public string cardName;
		public string targetNodeType;
		public CardType cardType;/*
		public Sprite art;
		public string cardDetail;

		public int manaCost;
		public int range;
		public Sprite cardType;
		public Sprite soundLocation;
		public Sprite AtkDef;:*/

		public CardProperties[] properties;
	}
}