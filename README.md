# About The Project
This game was created for the Online game design course. The goal was to create an online game after receiving a topic. It was developed in Unity 3D (C#).

# Description
Before creating the game, we produced the Game Desing Document with all of the aspects of the game. The game is an online card board game where the player can move to different cells and play some cards each turn. The goal is to get 3 keys and reach the well in the center of the map.

# Author
Vallauri Diego, Carlo Alberto Locco e Pierpaolo Serrano.

# Contacts
mail: diego.vallauri@gmail.com 
